Aruem은 boost.asio를 이용한 서버 프레임워크 입니다.

### 튜토리얼 ###

1. Event 생성
	
	사용자 API 추가시 Event 객체를 상속 받아 처리되어야 합니다. 
	
		class Login : public Event 
		{
			virtual void Process(uint32_t sid, uint8_t* data, size_t bytes)
			{
				// TODO: 처리 코드 추가
				ByteBuffer parser(data, bytes);
				std::wstring pid << parser;
				int32_t id << parser;
				std::wstring nickname << parser;
				
				Process(sid, pid, id, nickname);
			}
			
			void Process(uint32_t sid, const std::wstring& pid, int32_t& id, const std::wstring nickname)
			{
				// TODO: 처리 코드 추가
				
				// 응답 메시지 전송
				S2C::LoginResult(sid, ErrorCode::Ok);
			}
		}
	
2. EventDispatcher 생성 및 등록

	이베트를 생성 후 EventDispatcher에 등록합니다. 로직 쓰레드에서 해당 Event 호출 시 Dispatcher에 등록된 이벤트를 꺼내 콜 합니다.

		auto dispatcher = std::make_shared<EventDispatcher>();
		dispatcher->Register(PID::Login, std::make_shared<Login>());

3. Log 
	
	내부에 심플 로거가 구현되어 있으며 간단한 설정으로 해당 로거를 이용할수 있습니다.
	
	Logger의 특징으로는 비동기 로그 출력, 유니코드 지원, json 형식의 설정 파일을 통한 로그 출력 세팅등이 있습니다.
	
	3.1 Configuration
	
	- reporting_level - 보고 가능한 레벨
	
		입력 값 - TRADE, DEBUG, INFO, WARN, ERROR
		
	- pattern - 로그 출력 형식
		* %DT% - 날짜 ex) 2016-10-20
		* %TM% - 시간 ex) 18:19:20.1.02345
		* %LV% - 레벨
		* %FUNC% - 함수명
		* %TID% - 쓰레드 ID
		* %MSG% - 메시지
		
	- filename 경로 및 파일명 정보(파일명의 경우 날짜 패턴을 사용 가능합니다. 아래 링크에 Format Flags 참고)
	  (http://www.boost.org/doc/libs/1_60_0/doc/html/date_time/date_time_io.html)
	
	- Sample
	
		{
			"reporting_level":"DEBUG",
			"pattern":"[%TM%][%LV%][%TID%][%FUNC%] - %MSG%\n",
			"filename":"./log/%Y-%d-%m_%H.log"
		}
	
	3.2 설정 및 사용 예재
	
		int main() 
		{
			GLOG.Initialize("log.conf");
	
			GLOG.Info(__FUNCTION__, "Server Start ... Ok");
	
			return 0;
		}
	

4. 서버 객체 생성 및 내부 이벤트 감지

	4.1 서버 객체 생성	
		
		class Server : public aruem::network::Listener
		{
		public:
			Server()
			{
			}

			virtual void JoinClient(int32_t session_id) 
			{
				GLOG.Info(__FUNCTION__, "sid=%d", session_id);
				server_.SetSessionTag(session_id, (void*)&tag_);
			}

			virtual void LeaveClient(int32_t session_id, void* tag)
			{
			}

			virtual void Error(int32_t session_id, void* tag, boost::system::error_code& error_code)
			{
			}

			void Run(aruem::network::ServerParameter parameter)
			{ 
				GLOG.Info(__FUNCTION__, "server run\t[Ok]");

				server_.Attach(event_dispatcher_.event_dispatcher());
				server_.Attach((aruem::network::Listener*)this);

				server_.Start(parameter); 
			}

		private:
			Tag tag_;
			aruem::logic::EventDispatcherWrap event_dispatcher_;
			aruem::network::Server server_;
		};

	4.2 서버 구동
	
		int main() 
		{
			// .. 생략 ..
			
			Server server;
			server.Run(aruem::network::ServerParameter("11122"));
			
		
			return 0;
		}

5. Header 처리

	5.1 Binary 통신 시 Header 구성
	
		struct Header
		{
			static const uint16_t kPrefix = 0xFFFA;

			// ... 생략 ...

			uint16_t prefix; ///< 패킷 시작 값!
			uint16_t id; ///< 패킷 고유 ID
			uint8_t is_compress; ///< 압축 여부
			uint8_t is_encrypt; ///< 압호화 여부
			PacketHandlingMode packet_handling_mode; ///< 순서가 보장되야하는지 여부를 설정합니다. 기본값은 순서 보장입니다.
			uint32_t data_length; ///< 전달된 데이터의 길이
		};

	5.2 속성 값
	
		* prefix : 패킷 시작을 표시 합니다. 기본적으로 oxFFFA 값으로 세팅됩니다.
		* id : EventDispatcher에 등록되는 키 값 입니다.
		* is_compress : 압축 여부를 표시하며 gzip 압축을 사용하니다.
		* is_encrypt : 압호화 여부를 표시하며 기본적으로 CBC AES 압호화 방식이 사용됩니다.
		* packet_handling_mode : 패킷 제어 모드 이며 순서가 유지되야 하는 경우 Reliable 모드를 사용하며 비동기 모드일 경우 Unreliable을 사용합니다.
		* data_length : 전송할 데이터 크기 입니다.
	
6. 자동화(API Generator)

	ver 3.0 추가 예정

### 지원 프로토콜 ###

* Binary 
* HTTP Protocol(ver 0.3.0 지원 예정)

### 라이브러리 의존성 관리 ###

* NuGet 패키지 사용

### 외부 라이브러리 ###

* boost 1.62.0.0
* tbb 4.4.5
* cryptopp 5.6.5.4
* zlib 1.2.8.7

### 문서화 도구 ###

* doxygen 
