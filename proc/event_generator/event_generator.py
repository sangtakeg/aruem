import sys, getopt
import re
import string
from os import listdir
from os.path import isfile, join, splitext

'''
usage: event_generator.py -e <input path> -o <output path>
'''

event_dispatcher_text = """#pragma once

#include <memory>
#include "network/event_dispatcher.h"
#include "pid.h"
%s

namespace aruem
{
namespace logic
{
class EventDispatcherWrap
{
public:
    EventDispatcherWrap()
        : event_dispatcher_(new network::EventDispatcher)
    {
%s
    }

    std::shared_ptr<network::EventDispatcher>& event_dispatcher()
    {
        return event_dispatcher_;
    }

private:
    std::shared_ptr<network::EventDispatcher> event_dispatcher_;
};
} // namespace logic
} // namespace aruem
"""

pid_text = """#pragma once

namespace aruem
{
namespace logic
{
enum PID
{
    UNKNOWN = 0,
%s
};
} // namespace logic
} // namespace aruem
"""


def read_file_list(path):
    return [f for f in listdir(path) if isfile(join(path, f)) and bool(re.match("[\\w]+\.h", f))]

def generate(files):
    event_dispatcher_body = '\t\tevent_dispatcher_->Register(PID::%s, std::make_shared<%s>());'
    event_dispatcher_includes_element = "#include \"event/%s\""


    enumerate_content = []
    event_dispatcher_includes = []
    event_dispatcher_content = []
    for file in files:
        name = splitext(file)[0]
        enumerate_element = string.upper(name)
        enumerate_content.append('\t' + enumerate_element + ',')

        class_name = string.replace(name.title(), '_', '')

        event_dispatcher_includes.append(event_dispatcher_includes_element % file)
        event_dispatcher_content.append(event_dispatcher_body % (enumerate_element, class_name))

    event_dispatcher_wrap_file_content = event_dispatcher_text % (string.join(event_dispatcher_includes, '\n'), string.join(event_dispatcher_content, '\n'))
    pid_file_content =  pid_text % string.join(enumerate_content, '\n')

    return event_dispatcher_wrap_file_content, pid_file_content

def create_file(path, content):
    with open(path, 'w') as f:
        f.write(content)

def main(argv):
    event_path = ''
    output_path = ''

    try:
        opts, args = getopt.getopt(argv, "he:o:", ["epath=","opath"])
    except getopt.GetoptError:
        print 'except: usage: event_generator.py -e <input path> -o <output path>'
        sys.exit(2)

    for opt, arg in opts:
        if opt in ("-e", "--epath"):
            event_path = arg
        elif opt in ("-o", "--opath"):
            output_path = arg
        else:
            print 'except: usage: event_generator.py -e <input path> -o <output path>'
            sys.exit()

    event_dispatcher_includes_element = "#include \"event/%s\""
    print event_dispatcher_includes_element

    files = read_file_list(event_path)

    bodys = generate(files)
    create_file(join(output_path, 'event_dispatcher_wrap.h'), bodys[0])
    create_file(join(output_path, 'pid.h'), bodys[1])

if __name__ == "__main__":
    main(sys.argv[1:])
