#include <iostream>
#include "network/server.h"
#include "network/listener.h"
#include "logic/event_dispatcher_wrap.h"
#include "utility/log/log.h"
#include <tbb/concurrent_queue.h>
#include <tbb/concurrent_hash_map.h>
#include <vector>
#include <memory>
#include <string>
#include "utility/stop_watch.h"
#include <boost/variant.hpp>
#include "utility/function.h"
#include "utility/property.h"
#include "utility/stop_watch.h"
#include "utility/random_generator.h"

struct Tag
{
	long user_id;
	//ChannelManager channels;
	//UserManager users;
};

class Server : public aruem::network::Listener
{
public:
	Server()
	{
	}

	virtual void JoinClient(int32_t session_id) 
	{
		GLOG.Info(__FUNCTION__, "sid=%d", session_id);
		server_.SetSessionTag(session_id, (void*)&tag_);
	}

	virtual void LeaveClient(int32_t session_id, void* tag)
	{
	}

	virtual void Error(int32_t session_id, void* tag, boost::system::error_code& error_code)
	{
	}

	void Run(aruem::network::ServerParameter parameter)
	{ 
		GLOG.Info(__FUNCTION__, "server run\t[Ok]");

		server_.Attach(event_dispatcher_.event_dispatcher());
		server_.Attach((aruem::network::Listener*)this);
		
		server_.Start(parameter); 
	}

private:
	Tag tag_;
	aruem::logic::EventDispatcherWrap event_dispatcher_;
	aruem::network::Server server_;
};

int main() 
{
	GLOG.Initialize("log.conf");

	Server server;
	
	// ���� ����
	server.Run(aruem::network::ServerParameter("11122"));
	
	return 0;
}