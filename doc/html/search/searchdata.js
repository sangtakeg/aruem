var indexSectionsWithContent =
{
  0: "acdefghijlmoprstvwë",
  1: "acefhilprstvw",
  2: "acehlrstv",
  3: "acdefgijlmprstvw",
  4: "diops",
  5: "ls",
  6: "ai",
  7: "g",
  8: "ë",
  9: "asë"
};

var indexSectionNames =
{
  0: "all",
  1: "classes",
  2: "files",
  3: "functions",
  4: "variables",
  5: "enums",
  6: "enumvalues",
  7: "defines",
  8: "groups",
  9: "pages"
};

var indexSectionLabels =
{
  0: "All",
  1: "Classes",
  2: "Files",
  3: "Functions",
  4: "Variables",
  5: "Enumerations",
  6: "Enumerator",
  7: "Macros",
  8: "Modules",
  9: "Pages"
};

