var searchData=
[
  ['send',['Send',['../classaruem_1_1network_1_1_transfer_proxy.html#a6c0655009c29091180b83df85a1c1731',1,'aruem::network::TransferProxy::Send(uint32_t sid, Header &amp;&amp;header)'],['../classaruem_1_1network_1_1_transfer_proxy.html#a9f8b415f04f6a28c4ef261dfbcb6bef8',1,'aruem::network::TransferProxy::Send(uint32_t sid, Header &amp;&amp;header, uint8_t *data)'],['../classaruem_1_1network_1_1_transfer_proxy.html#a647fe0433505b7e92963d160326399fc',1,'aruem::network::TransferProxy::Send(uint32_t *sids, uint16_t number_of_sid, Header &amp;header)'],['../classaruem_1_1network_1_1_transfer_proxy.html#aab4a0b734354c9fd251150ae6a85f94a',1,'aruem::network::TransferProxy::Send(uint32_t *sids, uint16_t number_of_sid, Header &amp;header, uint8_t *data)']]],
  ['server',['Server',['../classaruem_1_1network_1_1_server.html#a27c4828c5c63dc948abcc497e45cce7c',1,'aruem::network::Server']]],
  ['session',['Session',['../classaruem_1_1network_1_1_session.html#a6b56c63f3ca7cad352ee0aac2215ec55',1,'aruem::network::Session']]],
  ['sessionmanager',['SessionManager',['../classaruem_1_1network_1_1_session_manager.html#a15cd02b70b67d75a0810c5365b8b199b',1,'aruem::network::SessionManager']]],
  ['setup',['Setup',['../classaruem_1_1network_1_1_event_dispatcher.html#a21da7d07f408210180c5990d6a3c1cbd',1,'aruem::network::EventDispatcher']]],
  ['socket',['socket',['../classaruem_1_1network_1_1_session.html#ac2dac84a6261ffffc42cf29c2ab0b696',1,'aruem::network::Session']]],
  ['start',['Start',['../classaruem_1_1network_1_1_server.html#a970109ff2f1308626d7bce4ea8c712a1',1,'aruem::network::Server']]],
  ['status',['status',['../classaruem_1_1network_1_1_session.html#a320f797ed4368172b33856d838f81094',1,'aruem::network::Session::status(Status status)'],['../classaruem_1_1network_1_1_session.html#ae998b49fc5615f1dcfb8d92d08fcf3cc',1,'aruem::network::Session::status()']]],
  ['stop',['Stop',['../classaruem_1_1network_1_1_server.html#a04ef0b32aa0947ed9a40721f3d34b0b5',1,'aruem::network::Server']]]
];
