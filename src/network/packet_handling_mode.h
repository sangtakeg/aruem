/**
@file header.h

@brief 패킷의 구분 정보

@author 권상택
@date 2016-09-09
@since 0.1.0
*/
#pragma once

namespace aruem
{
namespace network
{
/**
	@class PacketHandlingMode

	@brief 패킷 처리 모드를 정의 합니다.

	패킷 처리시 순서를 보장해야 하는경우 Relaible, 

	@author 권상택
	@date 2016-09-09
	@since 0.1.0
*/
enum class PacketHandlingMode : uint8_t
{
	Unknown,
	Reliable, ///< 패킷 처리 순서를 보장합니다.
	Unreliable ///< 패킷 처리 순서가 보장되지 않습니다.
};
}
}