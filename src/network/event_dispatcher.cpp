﻿#include "event_dispatcher.h"
#include <unordered_map>
#include "event.h"
#include "session_manager.h"
#include "session.h"

namespace aruem
{
namespace network
{
struct EventDispatcher::Impl
{
public:
	using Events = std::unordered_map<uint32_t, std::shared_ptr<Event>>;
	
	Events events_;
};

EventDispatcher::EventDispatcher()
	: pimpl_(new Impl)
{
}

EventDispatcher::~EventDispatcher() = default;

void EventDispatcher::Register(uint32_t pid, std::shared_ptr<Event> event)
{
	pimpl_->events_.insert(std::make_pair(pid, event));
}

void EventDispatcher::Dispatch(Tasks& tasks)
{
	auto session_opt = SessionManagerInstance.Lookup(tasks.session_id);
	if (!session_opt) // 발생할수 없지만 예외 처리!!
		return;
	
	auto iter = pimpl_->events_.find(tasks.packet_id);
	if (iter != pimpl_->events_.end())
	{
		(*iter).second->Process(tasks.session_id, tasks.data.GetDataConst(), tasks.data.GetDataLength(), tasks.tag);
	}
	else 
	{
		// 존재 하지 않는 패킷ID이므로 에러 등록하여 유저가 인지 할수 있게 함.
		session_opt.get()->SetError(ErrorCode::InvalidPacketID);
	}

	// Reliable Message일 경우 해당 처리를 합니다.
	if (tasks.PHM == PacketHandlingMode::Reliable)
	{
		session_opt.get()->TransferReliableMessage();
	}
}
} // namespace network
} // namespace aruem