﻿/**
	@file session.h

	@brief 서버에 연결된 유저의 연결 정보

	@author 권상택
	@date 2016-09-09
	@since 0.1.0
*/
#pragma once

#include <memory>
#include <boost/asio.hpp>
#include "error.h"
#include "header.h"

namespace aruem
{
namespace network
{
/**
	@file session.h

	@brief 서버에 연결된 유저의 연결 정보

	서버 내부적으로 연결 정보를 유지하기 위해 사용됩니다.\n\n
	클라이언트와 연결이 이뤄지게되면 연결된 클라이언트와 서버사이에 발생하는 이벤트는 해당 클래스 객체를 통해 알수 있습니다.

	@author 권상택
	@date 2016-09-09
	@since 0.1.0
*/
class Logic;
class Listener;
class Session
{
public:
	/**
		@brief Session의 상태를 나타냅니다.

		세션은 내부적으로 Pooling 하여 사용합니다. 이때 세션의 활성화 여부를 알기 위해 사용됩니다.\n\n
		해당 Status는 status 메쏘드를 이용해 제어됩니다.
	*/
	enum class Status 
	{
		IDLE,	///< 세션 비활성화
		ACTIVE	///< 세션 활성화
	};

	/**
		@brief 생성자

		객체 생성시 세션의 기본 정보를 세팅합니다.

		@param id 세션의 고유 ID(SID)
		@param io_service 
		@param event_dispatcher 이벤트 처리를 위해 Dispatcher를 Attach
		@param restore Close 이벤트 발생 시 세션을 반환하기 위한 함수 핸들러

		@since 0.1.0
	*/
	Session(
		uint32_t id, 
		boost::asio::io_service& io_service, 
		Listener* listener,
		std::function<void(uint32_t)> restore
	);

	~Session();

	/**
		@brief SessionID를 반환합니다.

		@return SessionID

		@since 0.1.0
	*/
	const uint32_t& id() const;

	/**
		@brief 소켓 객체 반환

		@return 소켓 객체

		@since 0.1.0
	*/
	boost::asio::ip::tcp::socket& socket() const;

	/**
		@brief 상태 변경

		@param status 상태 값

		@since 0.1.0
	*/
	void status(Status status);
	
	/**
		@brief 상태 값 반환

		@return status 값

		@since 0.1.0
	*/
	Status status();

	void tag(void* tag);

	void* tag();

	/**
		@brief 읽기 가능 상태로 전환합니다.
		@since 0.1.0
	*/
	void Read();

	/**
		@brief 현재 세션으로 패킷 전송
		@since 0.1.0
	*/
	void Write(Header& header, uint8_t* data);


	/**
		@brief 다음 Reliable 메시지를 Logic 쓰레드로 전달합니다.
	*/
	void TransferReliableMessage();

	/**
		@brief Session 이외의 곳에서 에러가 발생했을 경우 통보하기 위해 제공되는 메소드입니다.
		@since 0.1.0
	*/
	void SetError(ErrorCode error_code);

private:
	class Impl;
	std::shared_ptr<Impl> pimpl_;
};
} // namespace network
} // namespace aruem