﻿#include "session_manager.h"
#include <queue>
#include <boost/thread.hpp>
#include <boost/asio.hpp>
#include "session.h"
#include "listener.h"
#include "logic.h"

namespace aruem
{
namespace network
{
struct SessionManager::Impl
{
public:
	using Sessions = std::vector<std::shared_ptr<Session>>;

	Sessions sessions;
	std::queue<uint32_t> available_ids;
	Listener* listener;
};

class SessionManager::IOServicePool
{
public:
	explicit IOServicePool(std::size_t size);

	void Start();
	void Stop();

	boost::asio::io_service& io_service();

private:
	using IOServicePtr = std::shared_ptr<boost::asio::io_service>;
	using WorkPtr = std::shared_ptr<boost::asio::io_service::work>;

	std::vector<IOServicePtr> io_services_;
	std::vector<WorkPtr> works_;
	std::vector<std::shared_ptr<boost::thread>> threads_;

	std::size_t next_io_service_;
};

SessionManager::IOServicePool::IOServicePool(std::size_t size)
	: next_io_service_(0)
{
	for (std::size_t i = 0; i < size; ++i)
	{
		IOServicePtr io_service(new boost::asio::io_service);
		WorkPtr work(new boost::asio::io_service::work(*io_service));

		io_services_.push_back(io_service);
		works_.push_back(work);
	}
}

void SessionManager::IOServicePool::Start()
{
	for (std::size_t i = 0; i < io_services_.size(); ++i)
	{
		std::shared_ptr<boost::thread> thread(new boost::thread(
			boost::bind(&boost::asio::io_service::run, io_services_[i])));

		threads_.push_back(thread);
	}
}

void SessionManager::IOServicePool::Stop()
{
	for (std::size_t i = 0; i < io_services_.size(); ++i)
	{
		io_services_[i]->stop();
	}

	for (std::size_t i = 0; i < threads_.size(); ++i) 
	{
		threads_[i]->join();
	}
}

boost::asio::io_service& SessionManager::IOServicePool::io_service()
{
	boost::asio::io_service& io_service = *io_services_[next_io_service_];
	++next_io_service_;
	if (next_io_service_ == io_services_.size())
		next_io_service_ = 0;

	return io_service;
}

SessionManager::SessionManager()
	: pimpl_(new Impl)
{
}

SessionManager::~SessionManager() = default;

void SessionManager::Initialize(Listener* listener, uint32_t number_of_session, size_t number_of_worker)
{
	pimpl_->listener = listener;

	io_service_pool_.reset(new IOServicePool(number_of_worker));

	io_service_pool_->Start();
	
	for (uint32_t id = 1; id <= number_of_session; ++id)
	{
		pimpl_->sessions.push_back(
			std::make_shared<Session>(id, 
				io_service_pool_->io_service(),
				pimpl_->listener,
				[this](uint32_t id) {
					this->Restor(id);
				}
			)
		);

		pimpl_->available_ids.push(id);
	}
}

void SessionManager::Finalize()
{
	io_service_pool_->Stop();

	pimpl_->sessions.clear();
}

std::shared_ptr<Session> SessionManager::Acquire()
{
	uint32_t id = pimpl_->available_ids.front();
	pimpl_->available_ids.pop();

	auto& session = pimpl_->sessions[id - 1];
	if (session->status() == Session::Status::ACTIVE) 
	{
		// 로그 작업
		return nullptr;
	}

	session->status(Session::Status::ACTIVE);

	return session;
}

void SessionManager::Restor(uint32_t sid)
{
	if (sid > pimpl_->available_ids.size() || sid <= 0)
		return;

	pimpl_->sessions[sid - 1]->status(Session::Status::IDLE);

	pimpl_->available_ids.push(sid);
}

boost::optional<std::shared_ptr<Session>&> SessionManager::Lookup(uint32_t sid)
{
	using SessionOpt = boost::optional<std::shared_ptr<Session>&>;

	if (sid > pimpl_->available_ids.size() || sid <= 0)
		return SessionOpt();

	auto& session = pimpl_->sessions[sid - 1];
	if (session->status() != Session::Status::ACTIVE)
		return SessionOpt();

	return SessionOpt(session);
}
} // namespace network
} // namespace aruem