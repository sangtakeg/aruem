﻿#include "server.h"
#include <boost/bind.hpp>
#include <boost/thread.hpp>
#include "session.h"
#include "session_manager.h"
#include "event_dispatcher.h"
#include "transfer_proxy.h"
#include "listener.h"
#include "logic.h"

namespace aruem
{
namespace network
{
class Server::Impl
{
public:
	using IoServicePtr = std::shared_ptr<boost::asio::io_service>;
	using WorkPtr = std::shared_ptr<boost::asio::io_service::work>;

	Impl();

	boost::asio::io_service& accept_io_service() const;

	void listener(Listener* listener) { listener_ = listener; }
	const Listener* listener() const { return listener_; }

	void event_dispatcher(Server::EventDispatcherPtr& event_dispatcher) { event_dispatcher_ = event_dispatcher; }
	const EventDispatcherPtr& event_dispatcher() const { return event_dispatcher_; }

	bool Start(ServerParameter parameter);
	void Accept();

private:
	void AcceptComplete(std::shared_ptr<Session>& session, const boost::system::error_code& ec);

	Listener* listener_;
	IoServicePtr io_service_;
	WorkPtr work_;
	Server::EventDispatcherPtr event_dispatcher_;

	boost::asio::ip::tcp::acceptor acceptor_;
};

Server::Impl::Impl()
	: io_service_(new boost::asio::io_service)
	, work_(new boost::asio::io_service::work(*io_service_))
	, acceptor_(*io_service_)
{
}

boost::asio::io_service& Server::Impl::accept_io_service() const
{
	return *io_service_;
}

bool Server::Impl::Start(ServerParameter parameter)
{
	if (event_dispatcher_ == nullptr) 
		return false;

	boost::asio::ip::tcp::resolver resolver(*io_service_);
	boost::asio::ip::tcp::resolver::query query(parameter.ip, parameter.port);
	boost::asio::ip::tcp::endpoint endpoint = *resolver.resolve(query);

	acceptor_.open(endpoint.protocol());
	acceptor_.set_option(boost::asio::ip::tcp::acceptor::reuse_address(true));
	acceptor_.bind(endpoint);
	acceptor_.listen();

	SessionManagerInstance.Initialize(listener_, parameter.number_of_session, parameter.network_thread_count);
	LogicInstance.Run(event_dispatcher_, parameter.logic_thread_count);

	return true;
}

void Server::Impl::Accept() 
{
	std::shared_ptr<Session> session = SessionManagerInstance.Acquire();
	acceptor_.async_accept(
		session->socket(), 
		boost::bind(&Server::Impl::AcceptComplete, this, session, boost::asio::placeholders::error)
	);
}

void Server::Impl::AcceptComplete(std::shared_ptr<Session>& session, const boost::system::error_code& ec)
{
	if (!acceptor_.is_open())
		return;

	if (!ec)
	{
		if (listener_ != nullptr)
			listener_->JoinClient(session->id());

		session->Read();
	}
	
	Accept();
}

Server::Server()
	: pimpl_(new Impl)
{
}

Server::~Server() 
{
	Stop();
}

bool Server::Start(ServerParameter parameter)
{
	if (pimpl_->Start(parameter) == false)
		return false;

	pimpl_->Accept();

	// 비동기 처리로 바꿀 예정!!
	pimpl_->accept_io_service().run();

	return true;
}

void Server::Stop()
{
	LogicInstance.Stop();

	SessionManagerInstance.Finalize();

	pimpl_->accept_io_service().stop();
}

void Server::SetSessionTag(int session_id, void* tag)
{
	auto session_opt = SessionManagerInstance.Lookup(session_id);
	if (!session_opt)
		return;

	session_opt.get()->tag(tag);
}

void Server::Attach(Listener* listener)
{
	pimpl_->listener(listener);
}

void Server::Attach(EventDispatcherPtr& event_dispatcher)
{
	pimpl_->event_dispatcher(event_dispatcher);
}
} // namespace network
} // namespace aruem