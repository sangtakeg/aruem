﻿/**
	@file server.h

	@brief 서버 시작 및 종료

	@author 권상택
	@date 2016-09-09
	@since 0.1.0

	기본 적인 세팅 정보를 받아 서버를 시작합니다.
*/
#pragma once

#include <memory>
#include <boost/asio.hpp>
#include "server_parameter.h"

namespace aruem
{
namespace network
{
/**
	@class Server server.h network/server.h

	@brief 서버에서 필요한 기본 정보를 받아 서버를 구동합니다.

	@code
	int main()
	{
		// 유저가 정의한 패킷 등록
		std::shared_ptr<aruem::network::EventDispatcher> event_dispatcher(new aruem::network::EventDispatcher);
		event_dispatcher->Register(1, std::make_shared<Login>());

		// 서버 생성
		aruem::network::Server server(event_dispatcher, "127.0.0.1.0", "9999", 1000, 8);

		// 유저가 정의한 proxy 등록
		Proxy proxy;
		server.Attach(&proxy);

		// 서버 시작
		server.Start();
	}
	@endcode

	@author 권상택
	@date 2016 - 09 - 09
	@since 0.1.0
*/
class Listener;
class EventDispatcher;
class TransferProxy;
class Server : boost::noncopyable
{
public:
	using EventDispatcherPtr = std::shared_ptr<EventDispatcher>;
	
	/**
		@brief 기본 정보를 입력 받아 서버를 구동합니다.

		입력된 정보를 이용해 SessionManager 초기화를 진행하며, 소켓 옵션 세팅 후 서버를 Listen 상태로 만듭니다.

		@param event_dispatcher 유저가 정의한 event 정보
		@param address 유저가 정의한 주소
		@param port 유저가 정의한 포트 번호
		@param number_of_session 최대 동접
		@param number_of_worker 워커 쓰레드 갯수

		@return 현재 조합된 문자열의 길이
		@since 0.1.0	
	*/
	Server();

	~Server();

	/**
		@brief 서버를 시작합니다.

		@since 0.1.0
	*/
	bool Start(ServerParameter parameter);
	
	/**
		@brief 서버를 중지 합니다.

		@since 0.1.0
	*/
	void Stop();

	/**
		@brief 세션에 Tag를 연결합니다.

		@param session_id Tag를 연결하려는 세션ID

		@since 0.1.0
	*/
	void SetSessionTag(int session_id, void* tag);

	/**
		@brief Listener를 연결 합니다.

		@since 0.2.0 
	*/
	void Attach(Listener* listener);

	/**
		@brief EventDispatcher를 연결합니다.

		서버 시작 전 연결이되야 유저 패킷을 처리할수있습니다.
		EventDispatcher가 연결되지 않은 상태로 서버를 시작 할 수 없습니다.

		@since 0.2.0
	*/
	void Attach(EventDispatcherPtr& event_dispatcher);

	/**
	*/
	//void Attach(Listener* listener);

private:
	class Impl;
	std::unique_ptr<Impl> pimpl_;
};
} // namespace network
} // namespace aruem