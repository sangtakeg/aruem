#pragma once

namespace aruem
{
namespace network
{
enum ErrorCode
{
	Ok = 0,
	InvalidHeader = 100,
	CompressFail,
	UncomperessFail,
	EncryptFail,
	DecryptFail,
	InvalidPacketID,
	InternalBufferError,
};

class ServerErrorCategory : public boost::system::error_category
{
public:
	ServerErrorCategory() 
	{
		descriptions_.insert(std::make_pair(CompressFail, "failed to compress a send data."));
		descriptions_.insert(std::make_pair(UncomperessFail, "failed to uncompress a receive data."));
		descriptions_.insert(std::make_pair(EncryptFail, "failed to encrypt a send data."));
		descriptions_.insert(std::make_pair(DecryptFail, "failed to decrypt a receive data."));
	}

	virtual const char* name() const BOOST_SYSTEM_NOEXCEPT
	{
		return "ServerErrorCategory";
	}

	virtual std::string message(int32_t ev) const
	{
		auto it = descriptions_.find(ev);
		if (it == descriptions_.end()) 
			return "Unknown";

		return (*it).second;
	}

private:
	std::map<int32_t, std::string> descriptions_;
};
}
}

namespace boost
{
namespace system
{
template<> struct is_error_condition_enum<aruem::network::ErrorCode>
{
	static const bool value = true;
};
}
}