﻿/**
	@file event.h

	@brief Event 기본 클래스

	@author 권상택
	@date 2016-09-09
	@since 0.1.0
*/
#pragma once

#include <cstdint>
#include "transfer_proxy.h"

namespace aruem
{
namespace network
{
/**
	@class Event event.h network/event.h

	@brief Event 기본 클래스

	유저는 패킷을 구현할 경우 해당 Event객체를 상속받아 Process에 내용을 구현해야 합니다.\n\n
	그래야만 EventDispatcher에 등록 후 패킷을 정상 처리할수 있습니다.

	@author 권상택
	@date 2016-09-09
	@since 0.1.0
*/
class Event
{
public:
	/**
		@brief Event 내용 처리

		@since 0.1.0
	*/
	virtual void Process(uint32_t sid, const uint8_t* data, size_t bytes, void* tag) = 0;
};
} // namespace network
} // namespace sr