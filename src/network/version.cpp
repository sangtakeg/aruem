﻿#include "version.h"

static const int32_t kMajor = 0;
static const int32_t kMinor = 1;
static const int32_t kPatch = 0;

namespace aruem
{
namespace network
{
int32_t Version::major()
{
	return kMajor;
}

int32_t Version::minor()
{
	return kMinor;
}

int32_t Version::patch()
{
	return kPatch;
}

std::string Version::version()
{
	return std::string(std::to_string(kMajor) + "." + std::to_string(kMinor) + "." + std::to_string(kPatch));
}
}
}
