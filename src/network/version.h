﻿/**
	@file version.h

	@brief 서버의 버전 정보에 접근합니다.

	@author 권상택
	@date 2016-09-09
	@since 0.1.0
*/
#pragma once

#include <string>
#include <memory>
#include <stdint.h>

namespace aruem
{
namespace network
{
/**
	@class Version version.h network/version.h

	@brief 서버의 버전 정보에 접근합니다.

	예를 들어, version을 사용해서 현재 버전 번호를 문자열로 가져올 수 있으며\n\n
	아니면 각각 major, minor, patch 메소드를 호출해 각 버전을 가져올 수 있습니다.

	@author 권상택
	@date 2016-09-09
	@since 0.1.0
*/
class Version
{
public:
	/**
		@brief 서버의 메이저 버전을 리턴합니다.

		@since 0.1.0
	*/
	static int32_t major();

	/**
		@brief 서버의 마이너 버전을 리턴합니다.

		@since 0.1.0
	*/
	static int32_t minor();

	/**
		@brief 서버의 패치 버전을 리턴합니다.

		@since 0.1.0
	*/
	static int32_t patch();

	/**
		@brief 서버의 버전 정보를 문자열로 반환합니다.

		@return . 으로 구분한 버전 정보를 반환합니다. 예를 들어 1.0.1.0과 같음.

		@since 0.1.0
	*/
	static std::string version();
};
}
}