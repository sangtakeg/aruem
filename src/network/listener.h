﻿#pragma once

#include <memory>

namespace aruem
{
namespace network
{
class Listener
{
public:
	virtual void JoinClient(int32_t session_id) = 0;
	virtual void LeaveClient(int32_t session_id, void* tag) = 0;

	virtual void Error(int32_t session_id, void* tag, boost::system::error_code& error_code) = 0;
};
} // network
} // aruem

// session_manager 초기화, tag 입력