/**
	@file session_manager.h

	@brief 세션 객체를 풀링하여 관리 합니다.

	@author 권상택
	@date 2016-09-09
	@since 0.1.0
*/
#pragma once

#include <vector>
#include <memory>
#include <boost/optional.hpp>
#include <boost/serialization/singleton.hpp>

namespace aruem
{
namespace network
{
/**
	@class SessionManager session_manager.h network/session_manager.h

	@brief 세션 객체를 풀링하여 관리 합니다.

	내부적으로 Session에 필요한 IoServicePool을 제공하게 되며 워커 쓰레드 개수는 SessionManager에서 사용되는 io_service의 개수입니다.

	@author 권상택
	@date 2016-09-09
	@since 0.1.0
	@note 해당 클래스는 서버 내부에서 사용되는 클래스 입니다.
*/
class Session;
class Logic;
class Listener;
class SessionManager : public boost::serialization::singleton<SessionManager>
{
public:
	/**
		@brief 객체를 초기화 합니다.

		SessionManager에 필요한 객체 및 설정 정보를 받아 세팅합니다.
		
		@logic 
		@param number_of_session 최대 동접자 수

		@since 0.2.0
	*/
	void Initialize(Listener* listener, uint32_t number_of_session, size_t number_of_worker);

	/**
		@brief 종료 처리

		@since 0.1.0
	*/
	void Finalize();

	/**
		@brief 풀링 된 객체를 할당합니다.
		
		@return Session 객체 반환

		@since 0.1.0
	*/
	std::shared_ptr<Session> Acquire();

	/**
		@brief 사용이 끝난 객체를 회수 합니다.

		@param sid 회수 할 SessionID

		@since 0.1.0
	*/
	void Restor(uint32_t sid);

	/**
		@brief SessionID를 통해 해당 Session를 조회 합니다.

		@param sid 조회할 SessionID

		@since 0.1.0
	*/
	boost::optional<std::shared_ptr<Session>&> Lookup(uint32_t sid);

protected:
	SessionManager();
	~SessionManager();

private:
	class IOServicePool;
	std::unique_ptr<IOServicePool> io_service_pool_;

	struct Impl;
	std::unique_ptr<Impl> pimpl_;
};

#define SessionManagerInstance SessionManager::get_mutable_instance()
} // namespace network
} // namespace aruem