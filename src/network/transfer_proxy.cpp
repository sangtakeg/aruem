﻿#include "transfer_proxy.h"
#include "session_manager.h"
#include "session.h"

namespace aruem
{
namespace network
{
TransferProxy::TransferProxy()
{
}

TransferProxy::~TransferProxy() = default;

void TransferProxy::Send(uint32_t sid, Header& header)
{
	auto session = SessionManagerInstance.Lookup(sid);
	if (!session)
		return;

	(*session)->Write(std::move(header), nullptr);
}

void TransferProxy::Send(uint32_t sid, Header& header, uint8_t* data)
{
	auto session = SessionManagerInstance.Lookup(sid);
	if (!session)
		return;

	(*session)->Write(header, data);
}

void TransferProxy::Send(uint32_t* sids, uint16_t number_of_sid, Header& header)
{
	if (sids == nullptr || number_of_sid <= 0)
		return;

	for (int16_t i = 0; i < number_of_sid; ++i) 
	{
		auto session = SessionManagerInstance.Lookup(sids[i]);
		if (!session)
			return;

		(*session)->Write(std::forward<Header>(header), nullptr);
	}
}

void TransferProxy::Send(uint32_t* sids, uint16_t number_of_sid, Header& header, uint8_t* data)
{
	if (sids == nullptr || number_of_sid <= 0)
		return;

	for (int16_t i = 0; i < number_of_sid; ++i)
	{
		auto session = SessionManagerInstance.Lookup(sids[i]);
		if (!session)
			return;

		(*session)->Write(header, nullptr);
	}
}
} // namespace network
} // namespace aruem