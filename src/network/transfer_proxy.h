﻿/**
	@file transfer_proxy.h

	@brief 패킷 전송을 중개 합니다.

	@author 권상택
	@date 2016-09-09
	@since 0.1.0
*/
#pragma once

#include <cstdint>
#include <memory>
#include "header.h"

namespace aruem
{
namespace network
{
	/**
	@file TransferProxy transfer_proxy.h network/transfer_proxy.h

	@brief 패킷 전송을 중개 합니다.

	유저단에서 SessionID를 통해 패킷을 전송할수 있도록 중개 합니다.\n\n
	유저는 해당 클래스를 상속받아 전송에 필요한 패킷을 구현 후 Server::Attach를 통해 서버와 연결해주면 됩니다.

	@code
	class Proxy : public aruem::network::TransferProxy
	{
	public:
		// 로그인 결과 전송
		void LoginResult(uint32_t sid, int32_t error, int32_t channel)
		{
			packet::LoginResult proxy;
			proxy.set_error(error);
			proxy.set_channel(channel);

			std::size_t bytes = proxy.ByteSize();
			std::shared_ptr<uint8_t> out(new uint8_t[bytes]);
			proxy.SerializeWithCachedSizesToArray(out.get());

			Send(sid, Header{1000, bytes}, out.get());
		}

		// 로그 아웃 결과 전송
		void LogoutResult(uint32_t sid)
		{
			Send(sid, Header{1001, 0});
		}

		// 여러명에서 메시지 전송
		void SendMessage(uint32_t* sids, uint16_t number_of_sid)
		{
			Send(sids, number_of_sid, Header{1010, 0});
		}
	};
	@endcode

	@author 권상택
	@date 2016-09-09
	@since 0.1.0
	*/
class SessionManager;
class TransferProxy
{
public:
	TransferProxy();
	~TransferProxy();

	/**
		@brief 패킷을 전송합니다.

		데이터 없이 특정 세션으로 패킷을 전송합니다.

		@param sid Session ID
		@param header 패킷 전송에 필요한 헤더 정보

		@since 0.1.0
	*/
	void Send(uint32_t sid, Header& header);

	/**
		@brief 패킷을 전송합니다.

		특정 세션으로 데이터를 포함한 패킷을 전송합니다.

		@param sid Session ID
		@param header 패킷 전송에 필요한 헤더 정보
		@param data 패킷 데이터

		@since 0.1.0
	*/
	void Send(uint32_t sid, Header& header, uint8_t* data);

	/**
		@brief 패킷을 전송합니다.

		데이터 없이 특정 여러 세션으로 패킷을 전송합니다.

		@param sids SessionID 집합
		@param number_of_sid SessionID 집합의 수
		@param header 패킷 전송에 필요한 헤더 정보

		@since 0.1.0
	*/
	void Send(uint32_t* sids, uint16_t number_of_sid, Header& header);

	/**
		@brief 패킷을 전송합니다.

		특정 여러 세션으로 데이터를 포함한 패킷을 전송합니다.

		@param sids SessionID 집합
		@param number_of_sid SessionID 집합의 수
		@param header 패킷 전송에 필요한 헤더 정보
		@param data 패킷 데이터

		@since 0.1.0
	*/
	void Send(uint32_t* sids, uint16_t number_of_sid, Header& header, uint8_t* data);
};
} // namespace network
} // namespace aruem