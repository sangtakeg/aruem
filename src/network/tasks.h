﻿#pragma once

#include "packet_handling_mode.h"
#include "../utility/smart_buffer.h"

namespace aruem
{
namespace network
{
/**
@class Tasks

@brief 패킷 처리를 위한 데이터

EventDispatcher에서 데이터를 처리할수 있도록 정보를 구성 합니다.

@author 권상택
@date 2016-11-16
@since 0.1.0
*/
struct Tasks
{
	using Data = SmartBuffer<512>;

	int32_t session_id; ///< 세션 ID
	uint16_t packet_id; ///< 패킷 ID
	Data data; ///< 패킷 데이터
	PacketHandlingMode PHM; ///< PacketHandlingMode 참조
	void* tag; ///< Session에 연결된 객체 or 값
};
} // namespace aruem
} // namespace network