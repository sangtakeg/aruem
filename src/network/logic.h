﻿/**
@file logic.h

@brief Raw 패킷 분석된 데이터를 전달받아 Event 처리

@author 권상택
@date 2016-11-16
@since 0.1.0
*/
#pragma once

#include <memory>
#include <boost/serialization/singleton.hpp>
#include "tasks.h"

namespace aruem
{
namespace network
{
/**
@class Logic

@brief Raw 패킷 분석된 데이터를 전달받아 Event 처리

유저가 설정한 Logic 쓰레드를 가지게 되며, Queue를 이용해 데이터를 전달 받고
전달 받은 데이터를 EventDispatcher를 통해 처리하게 됩니다.

@author 권상택
@date 2016-11-16
@since 0.1.0
*/
class EventDispatcher;
class Logic : public boost::serialization::singleton<Logic>
{
public:
	/**
	@brief 객체 생성시 기본 정보 세팅과 로직 스레드를 생성하여 패킷 처리 준비를 합니다.

	@since 0.1.0
	*/
	void Run(std::shared_ptr<EventDispatcher>& event_dispatcher, size_t thread_count);

	/**
	@brief 로직 처리를 정지 합니다.

	@since 0.1.0
	*/
	void Stop();

	/**
	@brief Session(Worker Thread)에서 패킷을 전달 받습니다.

	@param tasks 처리를 위한 패킷 데이터

	@since 0.1.0
	*/
	void Transfer(const Tasks& tasks);

protected:
	Logic();
	~Logic() = default;
private:
	class Impl;
	std::shared_ptr<Impl> pimpl_;
};

#define LogicInstance Logic::get_mutable_instance()

} // namespace network
} // namespace aruem