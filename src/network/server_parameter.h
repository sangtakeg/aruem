﻿/**
@file server_parameter.h

@brief 서버 시작을 위한 세팅 정보

@author 권상택
@date 2016-11-17
@since 0.1.0
*/
#pragma once

#include <string>
#include <thread>

namespace aruem
{
namespace network
{
/**
@file server_parameter.h

@brief 서버 시작을 위한 세팅 정보

서버 시작을 위해 주소 정보, 최대 동접 수, 네트워크 쓰레드 수와 로직 쓰레드 수를 세팅합니다.

별도의 세팅을 하지 않을 경우 아래와 같은 값으로 기본 세팅 됩니다.

- ip = 127.0.0.1
- number_of_session = kDefaultSessionCount
- network_thread_count, logic_thread_count = 장비의 core 갯수

@author 권상택
@date 2016-11-17
@since 0.1.0
*/
struct ServerParameter
{
	static const int kDefaultSessionCount = 1000; ///< 별도 세팅이 없을 경우 기본적으로 세팅되는 최대 동접 수

	/**
	@brief 기본 생성자

	port를 제외하고 나머지 값들에 기본 값을 세팅합니다.

	@since 0.1.0
	*/
	ServerParameter() 
		: ip("127.0.0.1")
		, number_of_session(kDefaultSessionCount)
	{
		network_thread_count = std::thread::hardware_concurrency();
		logic_thread_count = std::thread::hardware_concurrency();
	}

	/**
	@brief 생성자

	port를 받아 세팅하고 나머지 값들은 기본값으로 세팅합니다.

	@since 0.1.0
	*/
	ServerParameter(std::string port)
		: ip("127.0.0.1")
		, port(port)
		, number_of_session(kDefaultSessionCount)
		, is_encrypt(false)
		, is_compress(false)
	{
		network_thread_count = std::thread::hardware_concurrency();
		logic_thread_count = std::thread::hardware_concurrency();
	}

	/**
	@brief 생성자

	ip와 port를 받아 세팅하고 나머지 값들은 기본값으로 세팅합니다.

	@since 0.1.0
	*/
	ServerParameter(std::string ip, std::string port)
		: ip(ip)
		, port(port)
		, number_of_session(kDefaultSessionCount)
		, is_encrypt(false)
		, is_compress(false)
	{
		network_thread_count = std::thread::hardware_concurrency();
		logic_thread_count = std::thread::hardware_concurrency();
	}

	std::string ip; ///< IP 주소 또는 도메인
	std::string port; ///< 접속 PORT
	uint32_t number_of_session; ///< 최대 동접 자 수
	size_t network_thread_count; ///< ASIO 워커 쓰레드 수
	size_t logic_thread_count; ///< 로직 쓰레드 수

	bool is_encrypt; ///< 패킷 암호화 여부
	bool is_compress; ///< 패킷 압축 여부
};
} // namespace network
} // namespace aruem