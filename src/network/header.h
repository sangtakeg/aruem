﻿/**
	@file header.h

	@brief 패킷의 구분 정보
	
	@author 권상택
	@date 2016-09-09
	@since 0.1.0
*/
#pragma once

#include <cstdint>
#include "packet_handling_mode.h"

namespace aruem
{
namespace network
{

/**
	@class Header

	@brief 패킷의 구분 정보

	패킷 처리시 패킷에 대한 기본적인 정보를 담습니다.

	@author 권상택
	@date 2016-09-09
	@since 0.1.0
*/
#pragma pack(push, 1)
struct Header
{
	static const uint16_t kPrefix = 0xFFFA;

	Header() 
		: prefix(kPrefix)
		, id(0)
		, is_compress(0)
		, is_encrypt(0)
		, data_length(0)
		, packet_handling_mode(PacketHandlingMode::Reliable)
	{
	}

	Header(uint16_t id, uint32_t data_length)
		: prefix(kPrefix)
		, id(id)
		, is_compress(0)
		, is_encrypt(0)
		, data_length(data_length)
		, packet_handling_mode(PacketHandlingMode::Reliable)
	{
	}

	Header(uint16_t id, uint8_t is_compress, uint8_t is_encrypt, uint32_t origin_data_length, uint32_t data_length)
		: prefix(kPrefix)
		, id(id)
		, is_compress(is_compress)
		, is_encrypt(is_encrypt)
		, data_length(data_length)
		, packet_handling_mode(PacketHandlingMode::Reliable)
	{
	}

	bool Valid() const
	{
		return prefix == kPrefix;
	}

	bool IsCompressed() 
	{
		return is_compress == 1;
	}

	bool IsEncrypt() 
	{
		return is_encrypt == 1;
	}

	bool IsReliable() 
	{
		return PacketHandlingMode::Reliable == packet_handling_mode;
	}

	uint16_t prefix; ///< 패킷 시작 값!
	uint16_t id; ///< 패킷 고유 ID
	uint8_t is_compress; ///< 압축 여부
	uint8_t is_encrypt; ///< 압호화 여부
	PacketHandlingMode packet_handling_mode; ///< 순서가 보장되야하는지 여부를 설정합니다. 기본값은 순서 보장입니다.
	uint32_t data_length; ///< 전달된 데이터의 길이
};
#pragma pack(pop)
} // namespace network
} // namespace aruem