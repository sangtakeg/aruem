﻿#include "session.h"
#include <functional>
#include <array>
#include <vector>
#include <iostream>
#include <boost/bind.hpp>
#include <boost/iostreams/filtering_stream.hpp>
#include <boost/iostreams/filter/gzip.hpp>
#include <boost/iostreams/stream.hpp>
#include <tbb/concurrent_queue.h>
#include <zlib.h>
#include "header.h"
#include "listener.h"
#include "logic.h"
#include "tasks.h"
#include "../utility/crypto/cbc_crypto.h"

namespace aruem
{
namespace network
{
enum class ReliableMessageStatus : int32_t
{
	Idle, ///< Reliable 메시지가 처리되고 있지 않음.
	Active ///< Reliable 메시지 처리 중
};

class ReliableMessages
{
public:
	bool Task(Tasks& task) 
	{
		return tasks_.try_pop(task);
	}

	void Put(const Tasks& task) 
	{
		tasks_.push(task);
	}

	void UpdateStatus(ReliableMessageStatus status) 
	{
		status_ = status;
	}

	bool IsActive()
	{
		return ReliableMessageStatus::Active == status_;
	}

private:
	ReliableMessageStatus status_;
	tbb::concurrent_queue<Tasks> tasks_;
};

class Session::Impl : public std::enable_shared_from_this<Session::Impl>
{
public:
	static const uint32_t kDataDefaultSize = 1024;
	
	using WriteQueue = tbb::concurrent_bounded_queue<std::shared_ptr<uint8_t>>;

	explicit Impl(uint32_t id, boost::asio::io_service& io_service, Listener* listener, std::function<void(uint32_t)>& restore);

	const uint32_t& id() const;
	boost::asio::ip::tcp::socket& socket();

	void status(Session::Status status);
	Session::Status status();

	void tag(void* tag);
	void* tag();

	void Read();
	void Write(std::shared_ptr<uint8_t>& data, size_t bytes);

	void Error(ErrorCode error_code);

	void TransferReliableMessage();

private:
	void Close(ErrorCode ec = ErrorCode::Ok);
	
	void ReadReliableMessage(Tasks& task);
	bool DecryptMessage(const uint8_t* src, std::size_t length, Tasks::Data& data);

	void ReadHeader(const boost::system::error_code& ec, std::size_t bytes_transferred);
	void ReadData(const boost::system::error_code& ec, std::size_t bytes_transferred);

	void WriteComplete(const boost::system::error_code& ec, std::size_t bytes_transferred);

	uint32_t id_;
	
	Session::Status status_;

	std::array<uint8_t, sizeof(Header)> header_;
	Tasks task_;
	
	std::size_t readed_head_length_;
	std::size_t readed_data_length_;

	std::function<void(uint32_t)> restore_;

	Listener* listener_;

	WriteQueue write_queue_;

	boost::asio::ip::tcp::socket socket_;

	ServerErrorCategory error_category_;

	ReliableMessages reliable_messages_;

	void* tag_;
};

Session::Impl::Impl(uint32_t id, boost::asio::io_service& io_service, Listener* listener, std::function<void(uint32_t)>& restore)
	: id_(id)
	, listener_(listener)
	, socket_(io_service)
	, restore_(restore)
	, status_(Session::Status::IDLE)
	, readed_data_length_(0)
	, tag_(nullptr)
{
}

const uint32_t& Session::Impl::id() const
{
	return id_;
}

boost::asio::ip::tcp::socket& Session::Impl::socket()
{
	return socket_;
}

void Session::Impl::status(Session::Status status)
{
	status_ = status;
}

Session::Status Session::Impl::status()
{
	return status_;
}

void Session::Impl::tag(void* tag)
{
	tag_ = tag;
}

void* Session::Impl::tag()
{
	return tag_;
}

void Session::Impl::TransferReliableMessage()
{
	Tasks task;
	if (reliable_messages_.Task(task) == false) 
	{
		reliable_messages_.UpdateStatus(ReliableMessageStatus::Idle);
		return;
	}

	LogicInstance.Transfer(task);
}

void Session::Impl::ReadReliableMessage(Tasks& task)
{
	if (reliable_messages_.IsActive() == true) 
	{
		reliable_messages_.Put(task);
		return;
	}

	LogicInstance.Transfer(task);

	reliable_messages_.UpdateStatus(ReliableMessageStatus::Active);
}

bool Session::Impl::DecryptMessage(const uint8_t* src, std::size_t length, Tasks::Data& data)
{
	std::string recover = CBC_Crypto<CryptoPP::AES>::Decrypt(src, length);
	if (recover.empty() == true)
	{
		Error(ErrorCode::DecryptFail);
		Read();
		return false;
	}

	uint8_t* buf = data.Reallocate(recover.size());
	if (buf == nullptr)
	{
		Error(ErrorCode::InternalBufferError);
		Read();
		return false;
	}

	memcpy(buf, recover.data(), recover.length());

	return true;
}

void Session::Impl::Read()
{
	readed_head_length_ = 0;
	readed_data_length_ = 0;

	socket_.async_read_some(
		boost::asio::buffer(header_, header_.max_size()),
		boost::bind(&Session::Impl::ReadHeader, 
			shared_from_this(),
			boost::asio::placeholders::error, 
			boost::asio::placeholders::bytes_transferred)
	);
}

void Session::Impl::Write(std::shared_ptr<uint8_t>& data, size_t bytes)
{
	write_queue_.push(data);
	
	boost::asio::async_write(socket_,
		boost::asio::buffer(data.get(), bytes),
		boost::bind(&Session::Impl::WriteComplete, shared_from_this(),
			boost::asio::placeholders::error,
			boost::asio::placeholders::bytes_transferred)
	);
}

void Session::Impl::ReadHeader(const boost::system::error_code& ec, std::size_t bytes_transferred)
{
	if (ec)
	{
		Close();
		return;
	}

	// EPoll LT에 대한 처리
	if ((bytes_transferred + readed_head_length_) < sizeof(Header)) 
	{
		readed_head_length_ += bytes_transferred;
		socket_.async_read_some(
			boost::asio::buffer(header_.data() + readed_head_length_, header_.max_size() - readed_head_length_),
			boost::bind(&Session::Impl::ReadHeader,
				shared_from_this(),
				boost::asio::placeholders::error,
				boost::asio::placeholders::bytes_transferred)
		);
		return;
	}

	const Header* header = (Header*)header_.data();
	if (!header->Valid())
	{
		Close(ErrorCode::InvalidHeader);
		return;
	}

	// task 세팅
	task_.session_id = id_;
	task_.packet_id = header->id;
	task_.data.Allocate(header->data_length);
	task_.PHM = header->packet_handling_mode;
	task_.tag = tag();

	boost::asio::mutable_buffers_1 buffer = boost::asio::buffer(task_.data.GetData(), task_.data.GetDataLength());
	
	// async_read_some 호출
	socket_.async_read_some(
		buffer,
		boost::bind(&Session::Impl::ReadData, 
			shared_from_this(),
			boost::asio::placeholders::error,
			boost::asio::placeholders::bytes_transferred
		)
	);
}

void Session::Impl::ReadData(const boost::system::error_code& ec, std::size_t bytes_transferred)
{
	if (ec)
	{
		Close();
		return;
	}

	Header* header = (Header*)header_.data();

	uint32_t length = header->data_length;

	// Linux(epoll) 사용시 레벨 트리거 처리!!
	if ((bytes_transferred + readed_data_length_) < length)
	{
		readed_data_length_ += bytes_transferred;

		boost::asio::mutable_buffers_1 buffer = boost::asio::buffer(task_.data.GetData() + readed_data_length_, length - readed_data_length_);
		
		socket_.async_read_some(
			buffer,
			boost::bind(&Session::Impl::ReadData,
				shared_from_this(),
				boost::asio::placeholders::error,
				boost::asio::placeholders::bytes_transferred
			)
		);

		return;
	}

	Tasks::Data& buffer = task_.data;

	// 암축 해제
	if (header->IsCompressed() == true || header->IsEncrypt() == true)  
	{
		if (header->IsCompressed() == true)
		{
			namespace IO = boost::iostreams;
			using Buffer_t = std::vector<uint8_t>;

			Buffer_t decompressed;

			IO::filtering_ostream os;
			os.push(IO::gzip_decompressor());
			os.push(std::back_inserter(decompressed));

			IO::write(os, (const char*)buffer.GetDataConst(), buffer.GetDataLength());
			IO::close(os);

			if (header->IsEncrypt() == true)
			{
				if (DecryptMessage(decompressed.data(), decompressed.size(), buffer) == false)
					return;
			} 
			else 
			{
				uint8_t* data = buffer.Reallocate(decompressed.size());
				if (data == nullptr) 
				{
					Error(ErrorCode::InternalBufferError);
					Read();
					return;
				}

				memcpy(data, decompressed.data(), decompressed.size());
			}
			
		}
		else if (header->IsEncrypt() == true && DecryptMessage(buffer.GetDataConst(), buffer.GetDataLength(), buffer) == false)
		{
			return;
		}
	}
	
	if (header->IsReliable() == true) 
	{	
		ReadReliableMessage(task_);
	}
	else
	{
		// 순서 보장이 필요 없을 경우 바로 Logic 쓰레드로 메시지를 전달합니다.
		LogicInstance.Transfer(task_);
	}

	Read();
}

void Session::Impl::WriteComplete(const boost::system::error_code& ec, std::size_t bytes_transferred)
{
	std::shared_ptr<uint8_t> completeData;
	write_queue_.try_pop(completeData);
	if (completeData != nullptr) {
		completeData.reset();
	}
}

void Session::Impl::Close(ErrorCode error_code)
{
	if (listener_ != nullptr) 
	{
		if (error_code != ErrorCode::Ok)
			Error(error_code);

		listener_->LeaveClient(id_, tag_);
	}

	restore_(id_);
}

void Session::Impl::Error(ErrorCode error_code)
{
	if (listener_ != nullptr) 
		listener_->Error(id(), tag(), boost::system::error_code{error_code, error_category_});
}

Session::Session(
	uint32_t id, 
	boost::asio::io_service& io_service, 
	Listener* listener,
	std::function<void(uint32_t)> restore
)	: pimpl_(new Impl(id, io_service, listener, restore))
{
}

Session::~Session() = default;

const uint32_t& Session::id() const
{
	return pimpl_->id();
}

boost::asio::ip::tcp::socket& Session::socket() const
{
	return pimpl_->socket();
}

void Session::status(Status status)
{
	pimpl_->status(status);
}

Session::Status Session::status()
{
	return pimpl_->status();
}

void Session::tag(void* tag)
{
	pimpl_->tag(tag);
}

void* Session::tag()
{
	return pimpl_->tag();
}

void Session::Read()
{
	pimpl_->Read();
}

void Session::Write(Header& header, uint8_t* data)
{
	size_t bytes = sizeof(header) + header.data_length;

	// TODO: 메모리 풀이 필요할수도 있으나 현재 OS에서 메모리 할당 알고리즘이 좋기 때문에 문제가 된다면 교체!!
	std::shared_ptr<uint8_t> out(new uint8_t[bytes]);
	if (data != nullptr) 
	{
		std::array<uint8_t, Impl::kDataDefaultSize> encrypt_buffer;

		uint8_t* result_data = data;
		if (header.IsEncrypt() == true)
		{
			size_t encrypt_buffer_length = encrypt_buffer.max_size();

			std::string cipher = CBC_Crypto<CryptoPP::AES>::Encrypt(data, header.data_length);
			
			result_data = (uint8_t*)cipher.data();
			header.data_length = static_cast<uint32_t>(cipher.length());

			header.is_encrypt = 1;
		}

		if (header.IsCompressed() == true) 
		{
			namespace IO = boost::iostreams;
			using Buffer_t = std::vector<uint8_t>;

			Buffer_t compressed;
			IO::filtering_ostream os;
			os.push(IO::gzip_compressor());
			os.push(std::back_inserter(compressed));
			
			IO::write(os, (const char*)result_data, header.data_length);
			IO::close(os);
			
			header.is_compress = 1;
		}
	}

	memcpy(out.get(), &header, sizeof(header));

	pimpl_->Write(out, bytes);
}

void Session::TransferReliableMessage()
{
	pimpl_->TransferReliableMessage();
}

void Session::SetError(ErrorCode error_code)
{
	pimpl_->Error(error_code);
}
} // namespace network
} // namespace aruem