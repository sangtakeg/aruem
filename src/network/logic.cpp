﻿#include "logic.h"
#include <iostream>
#include <thread>
#include <vector>
#include <tbb/concurrent_queue.h>
#include "event_dispatcher.h"

namespace aruem
{
namespace network
{
class Logic::Impl
{
public:
	void Put(const Tasks& tasks);
	void Task(Tasks& tasks);

	void Run(std::shared_ptr<EventDispatcher>& event_dispatcher, size_t thread_count);
	void Stop();

private:
	size_t thread_count_;
	std::vector<std::thread> thread_group_;

	volatile bool is_run_;

	std::shared_ptr<EventDispatcher> event_dispatcher_;

	tbb::concurrent_bounded_queue<Tasks> queue_;
};

void Logic::Impl::Put(const Tasks& tasks)
{
	queue_.push(tasks);
}

void Logic::Impl::Task(Tasks& tasks)
{
	queue_.pop(tasks);
}

void Logic::Impl::Run(std::shared_ptr<EventDispatcher>& event_dispatcher, size_t thread_count)
{
	event_dispatcher_ = event_dispatcher;
	thread_count_ = thread_count;

	for (int i = 0; i < thread_count_; ++i) {
		thread_group_.push_back(std::thread(
			[this]()
			{
				try 
				{
					while (is_run_) 
					{
						Tasks tasks;
						Task(tasks);

						if (is_run_ == false)
							break;

						event_dispatcher_->Dispatch(tasks);
					}
				} 
				catch (std::exception& e) 
				{
					std::cout << "Terminated logic thread. message=" << e.what() << std::endl;
				}
			})
		);
	}
}

void Logic::Impl::Stop()
{
	is_run_ = false;
	queue_.abort();

	for (int i = 0; i < thread_count_; ++i) 
	{
		if (thread_group_[i].joinable() == true)
			thread_group_[i].join();
	}
}

Logic::Logic()
	: pimpl_(new Impl)
{
}

void Logic::Run(std::shared_ptr<EventDispatcher>& event_dispatcher, size_t thread_count)
{
	pimpl_->Run(event_dispatcher, thread_count);
}

void Logic::Stop()
{
	pimpl_->Stop();
}

void Logic::Transfer(const Tasks& tasks)
{
	pimpl_->Put(tasks);
}
} // namespace network
} // namespace aruem