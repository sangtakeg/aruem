﻿/**
	@file event_dispatcher.h

	@brief 유저가 정의한 Event를 등록하고 Dispatch 합니다.
	
	@author 권상택
	@date 2016-09-09
	@since 0.1.0
*/
#pragma once

#include <cstdint>
#include <memory>
#include "tasks.h"

namespace aruem
{
namespace network
{
	/**
	@class EventDispatcher

	@brief 유저가 정의한 Event를 등록하고 Dispatch 합니다.

	유저가 정의한 Event를 등록 후 서버에 해당 객체를 Attach 합니다.\n\n
	해당 객체가 Server에 Attach 됐을 경우 PID를 통해 서버와 클라가 통신하게됩니다.

	@code
	class Login : public Event
	{
	public:
		virtual void Process(uint32_t sid, uint8_t* data, size_t bytes)
		{
			// TODO:: 로그인에 대한 패킷 처리를 진행합니다.
		}
	};

	int main()
	{
		auto dispatcher = std::make_shared<EventDispatcher>();
		dispatcher->Register(1000, std::make_shaed<Login>());

		Server server(dispatcher, "127.0.0.1.0, "9999", 1000, 8);

		// ... 생략 ...

		return 0;
	}
	@endcode

	@author 권상택
	@date 2016-09-09
	@since 0.1.0
	*/
class SessionManager;
class Event;
class EventDispatcher
{
public:
	EventDispatcher();
	~EventDispatcher();
	
	EventDispatcher(const EventDispatcher&) = default;
	EventDispatcher& operator=(EventDispatcher&) = default;
	
	/**
		@brief Event를 등록합니다.

		@param pid 해당 Event PacketID
		@param event Event를 상속 받은 객체

		@since 0.1.0
	*/
	void Register(uint32_t pid, std::shared_ptr<Event> event);

	/**
		@brief 해당 PID의 Event를 찾아 실행합니다.

		@param tasks 패킷 처리를 위한 데이터 및 정보

		@since 0.1.0
		@note 해당 메소드는 서버 내부에서 사용될 목적으로 작성되었습니다.
	*/
	void Dispatch(Tasks& tasks);
	
private:
	struct Impl;
	std::unique_ptr<Impl> pimpl_;
};
} // namespace network
} // namespace aruem