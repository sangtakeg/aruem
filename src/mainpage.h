﻿/**
	@mainpage SR Documents

	@section sec_Contents 콘텐츠

	@li @ref sec_Overview
	@li @ref sec_Detail
	@li @ref sec_SeeAlso

	@section sec_Overview 개요

	개요에 관란 내용 추가

	@section sec_Detail 구체적인 설명

	좀 더 구체적인 설명을 여기에 추가

	@section sec_SeeAlso 관련 항목

	@li @ref page_Logging
	@li @ref page_Versioning

	@page page_Logging 로깅 시스템
	로깅 기능에 관한 개요

	@link group_Loggin 모든 로깅 클래스 보기 @endlink

	@page page_Versioning API 버전 관리
	
	API 버전 관리에 관한 개요

	@link groupVersioning 모든 버전 관리 클래스 /endlink

	@defgroup group_Logging 로깅 기능 진단
	See @ref page_Logging 구체적인 설명

	@defgroup group_Versioning 버전 관리 시스템
	See @ref page_Versioning 구체적인 설명
*/