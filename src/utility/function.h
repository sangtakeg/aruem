/**
	@function.h

	@brief Class 멤버 함수를 참조하여 외부에서 사용가능하도록 돕습니다.

	@author 권상택
	@date 2016-09-09
	@since 0.1.0
*/

#pragma once

#include <type_traits>

namespace aruem
{
template <class Type> struct IsVoid : std::true_type {};
template <> struct IsVoid<void> : std::false_type {};

/**
	@class Function01

	@brief 반환타입과 아큐먼트를 갖는 메소드를 위임받아 사용합니다.

	@code
	class A
	{
	public:
		A() : val_(0) {}
		int Sum(int&& val) { val_ += val; std::cout << val_ << std::endl; }

	private:
		int val_;
	};

	class B
	{
	public:
		Function01<A, int> Delegate_Sum;
	};

	void main()
	{
		A a;
		B b;
		b.Delegate_Sum.Bind(&a, &A::Sum);

		b.Delegate_Sum(10);
	}

	@endcode

	@author 권상택
	@date 2016-12-21
	@since 0.1.0
*/
template <typename ThisClass, typename ReturnType, typename ReturnTypeEnable = void, typename... Args>
struct Function01;
template <typename ThisClass, typename ReturnType, typename... Args>
struct Function01<ThisClass, ReturnType, typename std::enable_if<IsVoid<ReturnType>::value>::type, Args...>
{
public:
	typedef ReturnType(ThisClass::*Callback)(Args&&... args);

	Function01()
		: object_(nullptr)
		, callback_(nullptr)
	{
	}

	void Bind(ThisClass* object, Callback callback)
	{
		object_ = object;
		callback_ = callback;
	}

	ReturnType operator()(Args&&... args)
	{
		return (*object_.*callback_)(std::forward<Args>(args)...);
	}

	ReturnType operator()(ThisClass* object, Args&&... args)
	{
		return (*object.*callback_)(std::forward<Args>(args)...);
	}

	bool IsEmpty()
	{
		return callback_ == nullptr;
	}

private:
	ThisClass* object_;
	Callback callback_;
};

/**
	@class Function02

	@brief 반환타임이 존재하는 메소드를 위임 받아 사용합니다.

	@code
	class A
	{
	public:
		A() : val_(0) {}
		bool IsTest() { return val_ == 0 ? false : true; }

	private:
		int val_;
	};

	class B
	{
	public:
		Function01<A, int> Delegate_IsTest;
	};

	void main()
	{
		A a;
		B b;
		b.Delegate_Sum.Bind(&a, &A::IsTest);

		b.Delegate_IsTest();
	}

	@endcode

	@author 권상택
	@date 2016-12-21
	@since 0.1.0
*/
template <typename ThisClass, typename ReturnType, typename ReturnTypeEnable = void>
struct Function02;

template <typename ThisClass, typename ReturnType>
struct Function02<ThisClass, ReturnType, typename std::enable_if<IsVoid<ReturnType>::value>::type>
{
public:
	typedef ReturnType(ThisClass::*Callback)();

	Function02()
		: object_(nullptr)
		, callback_(nullptr)
	{
	}

	void Bind(ThisClass* object, Callback callback)
	{
		object_ = object;
		callback_ = callback;
	}

	ReturnType operator()()
	{
		return (*object_.*callback_)();
	}

	ReturnType operator()(ThisClass* object)
	{
		return (*object.*callback_)();
	}

	bool IsEmpty()
	{
		return callback_ == nullptr;
	}

private:
	ThisClass* object_;
	Callback callback_;
};

/**
	@class Function03

	@brief 반환타입이 존재하지 않고 파라메터가 존재하는 메소드를 위암받아 사용합니다.

	@code
	class A
	{
	public:
		A() : val_(0) {}
		void SetVal(int&& val) { val_ == val; std::cout << val_ << std::endl; }

	private:
		int val_;
	};

	class B
	{
	public:
		Function01<A, int> Delegate_SetVal;
	};

	void main()
	{
		A a;
		B b;
		b.Delegate_Sum.Bind(&a, &A::IsTest);

		b.Delegate_SetVal(30);
	}

	@endcode

	@author 권상택
	@date 2016-12-21
	@since 0.1.0
*/
template <typename ThisClass, typename... Args>
struct Function03
{
public:
	typedef void (ThisClass::* Callback)(Args&&... args);

	Function03()
		: object_(nullptr)
		, callback_(nullptr)
	{
	}

	void Bind(ThisClass* object, Callback callback)
	{
		object_ = object;
		callback_ = callback;
	}
	
	void operator()(Args&&... args)
	{
		(*object_.*callback_)(std::forward<Args>(args)...);
	}

	void operator()(ThisClass* object, Args&&... args)
	{
		(*object.*callback_)(std::forward<Args>(args)...);
	}

	bool IsEmpty()
	{
		return callback_ == nullptr;
	}

private:
	ThisClass* object_;
	Callback callback_;
};

/**
	@class Function04

	@brief 반환타입과 파라메터가 존재하지 않은 메소드를 위암받아 처리합니다.

	@code
	class A
	{
	public:
		A() : val_(0) {}
		void Test() { if (val_ == 0) { std::cout << "yes" << std::endl; } else { std::cout << "no" << std::endl; } }

	private:
		int val_;
	};

	class B
	{
	public:
		Function04<A, int> Delegate_Test;
	};

	void main()
	{
		A a;
		B b;
		b.Delegate_Sum.Bind(&a, &A::IsTest);

		b.Delegate_Test();
	}

	@endcode

	@author 권상택
	@date 2016-12-21
	@since 0.1.0
*/
template <typename ThisClass>
struct Function04
{
public:
	typedef void(ThisClass::*Callback)();

	Function04()
		: object_(nullptr)
		, callback_(nullptr)
	{
	}

	void Bind(ThisClass* object, Callback callback)
	{
		object_ = object;
		callback_ = callback;
	}

	void operator()()
	{
		(*object_.*callback_)();
	}

	void operator()(ThisClass* object)
	{
		(*object.*callback_)();
	}

	bool IsEmpty()
	{
		return callback_ == nullptr;
	}

private:
	ThisClass* object_;
	Callback callback_;
};
}