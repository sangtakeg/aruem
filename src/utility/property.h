﻿/**
@file property.h

@brief 멤버 변수의 getter/setter를 관리합니다.

@author 권상택
@date 2016-09-09
@since 0.1.0
*/

#pragma once

template <typename T>
class Get
{
public:
	Get(T& value) : value_(value) {}

	T& operator()()
	{
		return value_;
	}

	const T& operator()() const
	{
		return value_;
	}

private:
	T& value_;
};

template <typename T>
class Set
{
public:
	Set(T& value) : value_(value) {}

	void operator()(T& value)
	{
		value_ = value;
	}

	void operator()(const T& value) const
	{
		value_ = value;
	}

private:
	T& value_;
};

template <typename T>
class GetSet
{
public:
	GetSet(T& value) : value_(value) {}

	T& operator()()
	{
		return value_;
	}

	const T& operator()() const
	{
		return value_;
	}

	void operator()(T& value)
	{
		value_ = value;
	}

	void operator()(const T& value) const
	{
		value_ = value;
	}

private:
	T& value_;
};

/**
@class Property

@brief 특정 타입의 변수를 참조로 받아 getter와 setter를 제공합니다.

@code
#include "Property.h"

class Sample
{
public:
Sample()
: get_value(get_value_)
, set_value(set_value_)
, get_set_value(get_set_value_)
{
}

Property<int, Get> get_value;
Property<int, Set> set_value;
Property<int, GetSet> get_set_value;

private:
int get_value_;
int set_value_;
int get_set_value_;
};

void main()
{
Sample sample;
sample.get_value(10); // error
int value = sample.get_value(); // ok

sample.set_value(20); // ok
value = sample.set_value() // error

sample.get_set_value(10); // ok
value = sample.get_set_value(); // ok
}
@endcode

@author 권상택
@date 2016-12-21
@since 0.1.0
*/
template <typename T, template<typename> typename Policy = GetSet>
class Property : public Policy<T>
{
public:
	Property(T& value)
		: Policy<T>(value)
	{
	}
};

#define PROPERTY(type, name) Property<type> name
#define PROPERTY_GET(type, name) Property<type, Get> name
#define Property_SET(type, name) Property<type, Set> name