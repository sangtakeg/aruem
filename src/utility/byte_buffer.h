#pragma once

#include <vector>

namespace aruem
{
class ByteBuffer
{
public:
	using type = uint8_t;
	using buffer_t = std::shared_ptr<uint8_t>;

		
	std::vector<type> v;
private:
	buffer_t buffer_;
};
} // namespace areum
