#pragma once

#include <boost/random.hpp>

template <typename Type, typename Distribution>
class RandomGenerator
{
public:
	typedef boost::mt19937_64 Engine;
	typedef boost::variate_generator<Engine&, Distribution> Generator;

	void SetSeed(time_t seed)
	{
		engine.seed(seed);
	}

	Type operator()(Type min, Type max)
	{
		if (min == max) 
			return Type();

		if (min > max) 
			return Type();

		return Generator(engine, Distribution(min, max))();
	}

private:
	Engine engine;
};

typedef RandomGenerator<float, boost::uniform_real<float> > FRandom;
typedef RandomGenerator<int, boost::uniform_int<int> > IRandom;

static FRandom frandom;
static IRandom irandom;