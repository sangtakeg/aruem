#pragma once

#include "crypto.h"

namespace aruem
{
template <typename CryptorType>
class CBC_Crypto
{
public:
	static std::string Encrypt(const uint8_t* plain, std::size_t length)
	{
		using namespace CryptoPP;

		uint8_t key[CryptorType::DEFAULT_KEYLENGTH] = {0, };
		uint8_t iv[CryptorType::BLOCKSIZE] = { 0x01, };

		CBC_Mode<CryptorType>::Encryption encryption(key, sizeof(key));

		return Crypto::Encrypt(encryption, plain, length);
	}

	static std::string Decrypt(const uint8_t* cipher, std::size_t length)
	{
		using namespace CryptoPP;

		uint8_t key[CryptorType::DEFAULT_KEYLENGTH] = { 0, };
		uint8_t iv[CryptorType::BLOCKSIZE] = { 0x01, };

		CBC_Mode<CryptorType>::Decryption decryption(key, sizeof(key));

		return Crypto::Encrypt(decryption, cipher, length);
		
	}
};
}