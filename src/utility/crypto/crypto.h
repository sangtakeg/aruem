#pragma once

#include <cryptopp/cryptlib.h>
#include <cryptopp/Base64.h>
#include <cryptopp/aes.h>
#include <cryptopp/modes.h>
#include <cryptopp/osrng.h>
#include <cryptopp/hex.h>

namespace aruem
{
class Crypto
{
public:
	template <typename Mode>
	static std::string Encrypt(Mode& encryptor, const uint8_t* plain, std::size_t length)
	{
		using namespace CryptoPP;

		std::string EncodedText;

		try {
			StringSource(plain, length, true,
				new StreamTransformationFilter(encryptor,
					new Base64Encoder(
						new StringSink(EncodedText), false
					), 
					BlockPaddingSchemeDef::ZEROS_PADDING
				)
			);
		}
		catch (...) 
		{
			return "";
		}

		return EncodedText;
	}

	template <typename Mode>
	static std::string Decrypt(Mode& decryptor, const uint8_t* cipher, std::size_t length)
	{
		using namespace CryptoPP;

		std::string RecoveredText;

		try {
			StringSource(cipher, length, true,
				new Base64Decoder(
					new StreamTransformationFilter(decryptor,
						new StringSink(RecoveredText),
						BlockPaddingSchemeDef::ZEROS_PADDING
					)
				)
			);
		}
		catch (...) 
		{
			return "";
		}

		return RecoveredText;
	}
};
} // namespace aruem