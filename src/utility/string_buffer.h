﻿/**
	@file string_buffer.h
	
	@brief 문자열을 연결한다.
	
	@author 권상택
	@date 2016-09-09
	@since 0.1.0
	
	문자열 더하기 연산을 효율적으로 하기 위한 도구 입니다.
*/

#pragma once

#include <type_traits>
#include <array>
#include <string>

namespace aruem
{
template <typename T>
struct is_character {};

template <>
struct is_character<char> 
	: public std::true_type 
{
};

template <>
struct is_character<wchar_t> 
	: public std::true_type 
{
};

template <typename ValueT, typename CharT>
struct float_to_string {};

template <typename ValueT>
struct float_to_string<ValueT, char>
{
	static std::string get(const char* fmt, ValueT val)
	{
		return std::_Floating_to_string<ValueT>(fmt, val);
	}
};

template <typename ValueT>
struct float_to_string<ValueT, wchar_t>
{
	static std::wstring get(const wchar_t* fmt, ValueT val)
	{
		return std::_Floating_to_string<ValueT>(fmt, val);
	}
};

template <typename CharT>
struct float_format_specifier {};

template <>
struct float_format_specifier<char> 
{
	static constexpr char* float_spec() { return "%f"; }
	static constexpr char* double_spec() { return "%f"; }
	static constexpr char* ldouble_spec() { return "%Lf"; }
};

template <>
struct float_format_specifier<wchar_t>
{
	static constexpr wchar_t* float_spec() { return L"%f"; }
	static constexpr wchar_t* double_spec() { return L"%f"; }
	static constexpr wchar_t* ldouble_spec() { return L"%Lf"; }
};


template <typename CharT, std::size_t Size, typename Enabled = void>
class string_buffer {};

/**
	@class string_buffer

	@brief 문자열 조합합니다.

	해당 클래스는 기본 자료 타입과 문자열을 \c operator<<()의 파라메터로 받아 하나의 문자열로 조합합니다.\n\n
	\c operator<<()를 통해 전달된 값들은 내부 버퍼에 복사되며 내부 버퍼의 경우 객체 생성시 템플릿 파라메터를 통해 할당됩니다.\n\n
	내부 버퍼의 할당은 객체가 생성될때 한번이며 추가적인 할당은 없습니다.\n\n
	만약 쓰기 중 할당된 내부 버퍼 범위를 벗어날 경우 std::out_of_range 예외를 던집니다.	

	@param CharT 내부 버퍼의 타입
	@param Size 내부 버퍼의 크기

	@author 권상택
	@date 2016-09-09
	@since 0.1.0
*/
template <typename CharT, std::size_t Size>
class string_buffer<CharT, Size, typename std::enable_if_t<is_character<CharT>::value>>
{
	static_assert(Size > 0, "Size is 0.");

public:
	using buffer_t = std::array<CharT, Size>;
	using size_type = std::size_t;
	using string_t = std::basic_string<CharT>;

	string_buffer()
		: offset_(0)
	{
	}
	
	string_buffer& operator<<(CharT val)
	{
		return copy_to_buffer(std::move(string_t(1, val)));
	}

	string_buffer& operator<<(int8_t val) 
	{
		return copy_to_buffer(
			std::move(std::_Integral_to_string<CharT>(val))
		);
	}

	string_buffer& operator<<(uint8_t val)
	{
		return copy_to_buffer(
			std::move(std::_Integral_to_string<CharT>(val))
		);
	}
	
	string_buffer& operator<<(int16_t val)
	{
		return copy_to_buffer(
			std::move(std::_Integral_to_string<CharT>(val))
		);
	}

	string_buffer& operator<<(uint16_t val)
	{
		return copy_to_buffer(
			std::move(std::_Integral_to_string<CharT>(val))
		);
	}

	string_buffer& operator<<(int32_t val)
	{
		return copy_to_buffer(
			std::move(std::_Integral_to_string<CharT>(val))
		);
	}

	string_buffer& operator<<(uint32_t val)
	{
		return copy_to_buffer(
			std::move(std::_Integral_to_string<CharT>(val))
		);
	}

	string_buffer& operator<<(int64_t val)
	{
		return copy_to_buffer(
			std::move(std::_Integral_to_string<CharT>(val))
		);
	}

	string_buffer& operator<<(uint64_t val)
	{
		return copy_to_buffer(
			std::move(std::_Integral_to_string<CharT>(val))
		);
	}

	string_buffer& operator<<(float val)
	{
		return copy_to_buffer(
			std::move(
				float_to_string<float, CharT>::get(float_format_specifier<CharT>::float_spec(), val)
			)
		);
	}

	string_buffer& operator<<(double val)
	{
		return copy_to_buffer(s
			td::move(
				float_to_string<float, CharT>::get(float_format_specifier<CharT>::double_spec(), val)
			)
		);
	}

	string_buffer& operator<<(long double val)
	{
		return copy_to_buffer(
			std::move(
				float_to_string<float, CharT>::get(float_format_specifier<CharT>::ldouble_spec(), val)
			)
		);
	}

	string_buffer& operator<<(string_t&& val)
	{
		return copy_to_buffer(std::forward<string_t>(val));
	}

	/**
		@brief 내부 버퍼의 최대 사이즈를 구합니다.

		@return 내부 버퍼의 최대 사이즈
		@since 0.1.0
	*/
	size_type max_size() 
	{
		return Size;
	}

	CharT* data() 
	{
		return buff_.data() + offset_;
	}

	/**
		@brief 현재 조합된 문자열의 길이를 구합니다.

		@return 현재 조합된 문자열의 길이
		@since 0.1.0
	*/
	size_type length() 
	{
		return offset_;
	}

	/**
		@brief 표준 string으로 문자열을 반환합니다.

		내부 버퍼에 조합된 문자열을 표준 문자열로 변환하여 반환합니다.

		@return 조합된 문자열을 표준 string으로 변환하여 반환합니다.
		@since 0.1.0
	*/
	string_t to_string() 
	{
		return string_t(buff_.data(), offset_);
	}

private:
	string_buffer& copy_to_buffer(string_t&& src)
	{
		size_type length = src.length() * sizeof(CharT);

		if ((offset_ + length) > Size)
			throw std::out_of_range("size over!");
		
		memcpy(buff_.data() + offset_, src.c_str(), length);
		offset_ += src.length();
		return *this;
	}

	size_type offset_;
	buffer_t buff_;
};
}