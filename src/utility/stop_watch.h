﻿/**
@file stop_watch.h

@brief elapse 시간을 계산 합니다.

@author 권상택
@date 2016-09-09
@since 0.1.0
*/

#pragma once

#include <memory>
#include <chrono>

/**
@class StopWatch

@brief Elpase Time을 계산 합니다.

@author 권상택
@date 2016-09-09
@since 0.1.0
*/
namespace aruem
{	
template <typename ReturnType, typename TimeType, typename Clock>
class StopWatch
{
public:
	template <typename Clock>
	using TimePoint = std::chrono::time_point<Clock>;

	StopWatch();

	void Start();

	ReturnType Stop();
	ReturnType Lap();
	ReturnType Reset();

private:
	bool is_start_;
	TimePoint<Clock> start_;
};

template <typename ReturnType, typename TimeType, typename Clock>
StopWatch<ReturnType, TimeType, Clock>::StopWatch()
{
	Start();
}

template <typename ReturnType, typename TimeType, typename Clock>
void StopWatch<ReturnType, TimeType, Clock>::Start()
{
	start_ = Clock::now();
	
	is_start_ = true;
}

template <typename ReturnType, typename TimeType, typename Clock>
ReturnType StopWatch<ReturnType, TimeType, Clock>::Lap()
{
	if (is_start_ == false)
		return ReturnType();

	using namespace std::chrono;
	return duration_cast<duration<ReturnType, TimeType>>(Clock::now() - start_).count();
}

template <typename ReturnType, typename TimeType, typename Clock>
ReturnType StopWatch<ReturnType, TimeType, Clock>::Stop()
{
	if (is_start_ == false)
		return ReturnType();

	is_start_ = false;

	return Split();
}

template <typename ReturnType, typename TimeType, typename Clock>
ReturnType StopWatch<ReturnType, TimeType, Clock>::Reset()
{
	is_start_ = false;
}

template <typename ReturnType>
using StopWatchS = StopWatch<ReturnType, std::ratio<1>, std::chrono::system_clock> ;

template <typename ReturnType>
using StopWatchM = StopWatch<ReturnType, std::milli, std::chrono::high_resolution_clock>;
}
