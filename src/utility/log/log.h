﻿/**
	@file log.h

	@brief Logger를 직관적으로 사용할수 있도록한 Wrapper 클래스 입니다.

	@author 권상택
	@date 2016-09-09
	@since 0.1.0
*/
#pragma once

#include <boost/serialization/singleton.hpp>
#include "detail/logger.h"

namespace aruem
{
/**
	@class Log

	@brief  Logger를 직관적으로 사용할수 있도록한 Wrapper 클래스 입니다.

	Logger 의 기능을 레벨 별로 분리하여 사용 가능하도록 기능을 제공합니다. \n\n
	또한 싱글톤으로 되어 있어 객체 생성 없이 헤더 추가 후 바로 사용가능한 전역 로그 입니다.

	@author 권상택
	@date 2016-09-09
	@since 0.1.0
*/
template <typename CharT>
class Log : public boost::serialization::singleton<Log<CharT>>
{
public:
	using string_t = std::basic_string<CharT>;

	/**
		@brief Configure 파일을 읽어 Logger를 초기화 합니다.

		@param filename Configure 파일명

		@since 0.1.0
		@note main 등에서 사용전 호출되야 합니다.
	*/
	void Initialize(std::string&& filename)
	{
		logger_.Initialize(std::forward<std::string>(filename));
	}

	/**
		@brief Debug 레벨 로그를 출력합니다.

		@param func 호출된 메소드명
		@param format 출력 형식
		@param arg 출력 형식에 사용될 값

		@since 0.1.0
	*/
	template<typename... Argc> void Debug(const string_t func, const CharT* format, Argc&&... arg)
	{
		logger_.Log(Level::Debug, func, format, std::forward<Argc>(arg)...);
	}

	/**
		@brief Info 레벨 로그를 출력합니다.

		@param func 호출된 메소드명
		@param format 출력 형식
		@param arg 출력 형식에 사용될 값

		@since 0.1.0
	*/
	template<typename... Argc> void Info(const string_t func, const CharT* format, Argc&&... arg)
	{
		logger_.Log(Level::Info, func, format, std::forward<Argc>(arg)...);
	}

	/**
		@brief Warn 레벨 로그를 출력합니다.

		@param func 호출된 메소드명
		@param format 출력 형식
		@param arg 출력 형식에 사용될 값

		@since 0.1.0
	*/
	template<typename... Argc> void Warn(const string_t func, const CharT* format, Argc&&... arg)
	{
		logger_.Log(Level::Warn, func, format, std::forward<Argc>(arg)...);
	}

	/**
		@brief Error 레벨 로그를 출력합니다.

		@param func 호출된 메소드명
		@param format 출력 형식
		@param arg 출력 형식에 사용될 값

		@since 0.1.0
	*/
	template<typename... Argc> void Error(const string_t func, const CharT* format, Argc&&... arg)
	{
		logger_.Log(Level::Error, func, format, std::forward<Argc>(arg)...);
	}

private:
	log::Logger<CharT> logger_; ///< Logger 객체
};
}

/**
	@brief 사용을 돕기 위한 멀티바이트용 로그 객체
	@since 0.1.0
*/
#define GLOG aruem::Log<char>::get_mutable_instance()

/**
	@breif Debug 로깅을 돕기 위한 멀티바이트용 로그 객체
	@code
	GLOG_DEBUG(__FUNCTION__, "test log message");
	@endcode
	@since 0.1.0
*/
#define GLOG_DEBUG GLOG.Debug

/**
	@breif Info 로깅을 돕기 위한 멀티바이트용 로그 객체
	@code
	GLOG_INFO(__FUNCTION__, "test log message");
	@endcode
	@since 0.1.0
*/
#define GLOG_INFO GLOG.Info

/**
	@breif warn 로깅을 돕기 위한 멀티바이트용 로그 객체
	@code
	GLOG_WARN(__FUNCTION__, "test log message");
	@endcode
	@since 0.1.0
*/
#define GLOG_WARN GLOG.Warn

/**
	@breif Error 로깅을 돕기 위한 멀티바이트용 로그 객체
	@code
	GLOG_ERROR(__FUNCTION__, "test log message");
	@endcode
	@since 0.1.0
*/
#define GLOG_ERROR GLOG.Error

/**
	@brief 사용을 돕기 위한 유니코드용 로그 객체
*/
#define GLOGW aruem::Log<wchar_t>::get_mutable_instance()

/**
	@breif Debug 로깅을 돕기 위한 유니코드용 로그 객체
	@code
	GLOGW_DEBUG(__FUNCTIONW__, L"test log message");
	@endcode
	@since 0.1.0
*/
#define GLOGW_DEBUG GLOGW.Debug

/**
	@breif Info 로깅을 돕기 위한 유니코드용 로그 객체
	@code
	GLOGW_INFO(__FUNCTIONW__, L"test log message");
	@endcode
	@since 0.1.0
*/
#define GLOGW_INFO GLOGW.Info

/**
	@breif warn 로깅을 돕기 위한 유니코드용 로그 객체
	@code
	GLOGW_WARN(__FUNCTIONW__, L"test log message");
	@endcode
	@since 0.1.0
*/
#define GLOGW_WARN GLOGW.Warn

/**
	@breif warn 로깅을 돕기 위한 유니코드용 로그 객체
	@code
	GLOGW_ERROR(__FUNCTIONW__, L"test log message");
	@endcode
	@since 0.1.0
*/
#define GLOGW_ERROR GLOGW.Error
