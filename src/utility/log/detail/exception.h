﻿/**
	@file exception.h

	@brief Configure 파싱 중 에러가 발생할 경우 발생된는 예외입니다.

	@author 권상택
	@date 2016-09-09
	@since 0.1.0
*/
#pragma once

#include <exception>

namespace aruem
{
namespace log
{
/**
	@class parse_configure_error parse_configure_error.h utiltiry/log/detail/parse_configure_error.h

	@brief Configure 파싱 중 에러가 발생할 경우 발생된는 예외입니다.

	Logger::Initialize 실행 중 Configure 파일 파싱 에러가 발생하면 해당 예외가 던져집니다.

	@author 권상택
	@date 2016-09-09
	@since 0.1.0
*/
class parse_configure_error : public std::exception
{
public:
	explicit parse_configure_error(char const* const _Message) throw()
		: exception(_Message)
	{
	}
};
} // namespace log
} // namespace sr
