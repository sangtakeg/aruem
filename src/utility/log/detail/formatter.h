﻿/**
	@file formmater.h

	@brief 패턴을 처리하기 위한 유틸

	@author 권상택
	@date 2016-09-09
	@since 0.1.0
*/
#pragma once

#include <ctime>
#include <string>
#include <vector>
#include <cstring>
#include <thread>
#include <boost/date_time.hpp>
#include "level.h"
#include "../../string_buffer.h"

#pragma warning(disable : 4996)

namespace aruem
{
namespace log
{
template <typename CharT>
struct pattern_specifier {};

template <>
struct pattern_specifier<char>
{
	static constexpr char prefix() { return '%'; }
	static constexpr char* time() { return "TM"; }
	static constexpr char* date() { return "DT"; }
	static constexpr char* func() { return "FUNC"; }
	static constexpr char* tid() { return "TID"; }
	static constexpr char* level() { return "LV"; }
	static constexpr char* message() { return "MSG"; }
};

template <>
struct pattern_specifier<wchar_t>
{
	static constexpr wchar_t prefix() { return L'%'; }
	static constexpr wchar_t* time() { return L"TM"; }
	static constexpr wchar_t* date() { return L"DT"; }
	static constexpr wchar_t* func() { return L"FUNC"; }
	static constexpr wchar_t* tid() { return L"TID"; }
	static constexpr wchar_t* level() { return L"LV"; }
	static constexpr wchar_t* message() { return L"MSG"; }
};

template <typename CharT>
struct pattern_form {};

template <>
struct pattern_form<char>
{
	static constexpr char* get()
	{
		return "[%TM%][%LV%][%TID%][%FUNC%] - %MSG%\n";
	}
};

template <>
struct pattern_form<wchar_t>
{
	static constexpr wchar_t* get()
	{
		return L"[%TM%][%LV%][%TID%][%FUNC%] - %MSG%\n";
	}
};

namespace datetime
{
	template <typename CharT>
	struct facet_fmt {};

	template <>
	struct facet_fmt<char>
	{
		static constexpr char* date_fmt() { return "%Y-%m-%d"; }
		static constexpr char* time_fmt() { return "%H:%M:%S.%f"; }
		static constexpr char* datetime_fmt() { return "%Y-%m-%d %H:%M:%S.%f"; }
	};

	template <>
	struct facet_fmt<wchar_t>
	{
		static constexpr wchar_t* date_fmt() { return L"%Y-%m-%d"; }
		static constexpr wchar_t* time_fmt() { return L"%H:%M:%S.%f"; }
		static constexpr wchar_t* datetime_fmt() { return L"%Y-%m-%d %H:%M:%S.%f"; }
	};

	template <typename CharT>
	std::basic_string<CharT> to_string(std::basic_string<CharT>&& fmt)
	{
		using facet_t = boost::date_time::time_facet<boost::posix_time::ptime, CharT>;
		using sstream_t = std::basic_stringstream<CharT, std::char_traits<CharT>, std::allocator<CharT>>;
		
		facet_t* facet = new facet_t(fmt.c_str());

		sstream_t time_stream;
		time_stream.imbue(std::locale(time_stream.getloc(), facet));
		time_stream << boost::posix_time::microsec_clock::local_time();
		
		return time_stream.str();
	}
}

template <typename CharT>
struct tid_fmt {};

template <>
struct tid_fmt<char>
{
	static std::string to_string()
	{
		std::thread::id tid = std::this_thread::get_id();
		std::stringstream ss;
		ss << "0x" << std::uppercase << std::setfill('0') << std::setw(8) << std::hex << tid;
		return ss.str();
	}
};

template <>
struct tid_fmt<wchar_t>
{
	static std::wstring to_string()
	{
		std::thread::id tid = std::this_thread::get_id();
		std::wstringstream ss;
		ss << L"0x" << std::uppercase << std::setfill(L'0') << std::setw(8) << std::hex << tid;
		return ss.str();
	}
};

template <typename CharT>
struct level_fmt {};

template <>
struct level_fmt<char>
{
	static const char* to_string(Level level)
	{
		switch (level)
		{
		case Level::Trace:
			return "TRACE";

		case Level::Debug:
			return "DEBUG";

		case Level::Info:
			return "INFO";

		case Level::Warn:
			return "WARN";

		case Level::Error:
			return "ERROR";
		}

		return "Unkown";
	}

	static const Level get(std::string level)
	{
		boost::to_upper(level);

		if (level.compare("TRACE") == 0)
			return Level::Trace;
		else if (level.compare("INFO") == 0)
			return Level::Info;
		else if (level.compare("WARN") == 0)
			return Level::Warn;
		else if (level.compare("ERROR") == 0)
			return Level::Error;

		return Level::Unkown;
	}
};

template <>
struct level_fmt<wchar_t>
{
	static const wchar_t* to_string(Level level)
	{
		switch (level)
		{
		case Level::Trace:
			return L"TRACE";

		case Level::Debug:
			return L"DEBUG";

		case Level::Info:
			return L"INFO";

		case Level::Warn:
			return L"WARN";

		case Level::Error:
			return L"ERROR";
		}

		return L"Unkown";
	}

	static const Level get(std::wstring level)
	{
		boost::to_upper(level);

		if (level.compare(L"TRACE") == 0)
			return Level::Trace;
		else if (level.compare(L"INFO") == 0)
			return Level::Info;
		else if (level.compare(L"WARN") == 0)
			return Level::Warn;
		else if (level.compare(L"ERROR") == 0)
			return Level::Error;

		return Level::Unkown;
	}
};

template <typename CharT>
struct StringLength {};

template <>
struct StringLength<char>
{
	static size_t Get(const char* s)
	{
		return strlen(s);
	}
};

template <>
struct StringLength<wchar_t>
{
	static size_t Get(const wchar_t* s)
	{
		return wcslen(s);
	}
};


template <typename CharT, typename... Arg>
struct string_util {

	using buffer_t = string_buffer<CharT, 1024>;

	static std::string format(const CharT* format, Arg&&... arg)
	{
		buffer_t out;

		size_t size = StringLength<CharT>::Get(format);

		size_t index = 1;

		for (int i = 0; i < size; ++i)
		{
			CharT ch = format[i];

			if (ch == '{' && ((i + 1) < size))
			{
				CharT next = format[i + 1];
				if (next == '}')
				{
					if (apply(index++, out, arg...) == true)
					{
						++i;
						continue;
					}
				}
			}

			out << ch;
		}
		return out.to_string();
	}

private:
	static bool apply(size_t Idx, buffer_t& buffer)
	{
		return false;
	}

	template <typename Ty, typename... Args>
	static bool apply(size_t Idx, buffer_t& buffer, Ty t, Args&&... rest)
	{
		if (Idx == 1)
		{
			buffer << t;
			return true;
		}

		return apply(Idx - 1, buffer, rest...);
	}
};
}
}