﻿/**
	@file logger.h

	@brief 파일 및 표준 출력으로 로그를 출력합니다.

	@author 권상택
	@date 2016-09-09
	@since 0.1.0
*/
#pragma once

#include <string>
#include <vector>
#include <memory>
#include <boost/property_tree/ptree.hpp>
#include <boost/property_tree/json_parser.hpp>
#include <boost/foreach.hpp>
#include <tbb/concurrent_queue.h>
#include "formatter.h"
#include "reporter.h"
#include "exception.h"
#include "../../string_buffer.h"

namespace aruem
{
namespace log
{
/**
	@class Logger

	@brief 심플 Logger 입니다.

	Logger의 특징으로는 비동기 로그 출력, 유니코드 지원, json 형식의 설정 파일을 통한 로그 출력 세팅등이 있습니다.\n\n
	로그 설정 파일의 속성으로는 
	- reporting_level - 보고 가능한 레벨\n
		입력 값 - TRADE, DEBUG, INFO, WARN, ERROR\n\n
	- pattern - 로그 출력 형식\n
		* %DT% - 날짜 ex) 2016-10-20\n
		* %TM% - 시간 ex) 18:19:20.1.02345\n
		* %LV% - 레벨\n
		* %FUNC% - 함수명\n
		* %TID% - 쓰레드 ID\n
		* %MSG% - 메시지\n\n
	- filename 경로 및 파일명 정보(파일명의 경우 날짜 패턴을 사용 가능합니다. 아래 링크에 Format Flags 참고)\n
	@link http://www.boost.org/doc/libs/1_60_0/doc/html/date_time/date_time_io.html @endlink
	
	@code
		[설정 파일 샘플]

		{
			"reporting_level":"DEBUG",
			"pattern":"[%TM%][%LV%][%TID%][%FUNC%] - %MSG%\n",
			"filename":"./log/%Y-%d-%m_%H.log"
		}
	@endcode

	@param CharT 내부적으로 사용한 캐릭터 타입

	@code
		void main() 
		{
			using namespace aruem::log;

			Logger<char> logger;
			try
			{
				logger.Initialize("settings.conf");
			}
			catch (parse_configure_error& e) 
			{
			}

			logger.Log(Level.INFO, __FUNCTION__, "test log");
		}
	@endcode

	@author 권상택
	@date 2016-09-09
	@since 0.1.0
*/
template <typename CharT>
class Logger
{
public:
	using string_t = std::basic_string<CharT>;

	Logger() 
		: worker_(new Worker)
		, configure_(new Configure)
	{	
	}

	~Logger()
	{
		Finalize();
	}

	/**
		@brief Logger를 초기화 합니다.

		\c Initialize는 설정 파일의 경로를 입력받아 설정 값을 적용하며 비동기 처리를 위한 워커 쓰레드를 생성합니다.

		@param path 설정 파일 경로
		@exception parse_configure_error 설정 파일 파싱 중 문제 발생
		@since 0.1.0
	*/
	void Initialize(std::string&& path)
	{
		configure_->Load(std::forward<std::string>(path));

		reporters_.push_back(std::make_shared<FileReporter<CharT>>());
		reporters_.push_back(std::make_shared<StandardOutReporter<CharT>>());

		auto file_reporter = static_cast<FileReporter<CharT>*>(reporters_.front().get());
		file_reporter->filename(configure_->filename);

		worker_->Run(reporters_);
	}

	/**
		@brief Logger를 종료 합니다.

		@since 0.1.0
	*/
	void Finalize()
	{
		worker_->Stop();
	}

	/**
		@brief 유저가 작성한 ReporterImpl를 추가합니다.

		@since 0.1.0
	*/
	void Attach(std::shared_ptr<ReporterImpl<CharT>>& reporter)
	{
		reporters_.push_back(reporter);
	}

	/**
		@brief 로그를 구성하며 구성된 메시지를 워커 쓰레드로 전달합니다.

		@param level 로그 레벨
		@param func 출력 함수 이름
		@param format 문자열 형식
		@param arg format에 사용된 값들

		@since 0.1.0
		@note 입력된 level이 설정 파일의 reporting_level보다 아래라면 입력된 내용을 출력하지 않습니다.
		@note 내부에 할당된 버퍼보다 큰 데이터가 입력됐을 경우 메시지를 무시 합니다.
	*/
	template <typename... Argc>
	void Log(Level level, string_t func, const CharT* format, Argc&&... arg) 
	{
		using size_type = string_t::size_type;
		using buffer_t = string_buffer<CharT, 2048>;
		using specif_t = pattern_specifier<CharT>;

		// 보고 레벨을 체크 합니다.
		if (configure_->reporting_level > level)
			return;

		buffer_t out;
		try 
		{
			for (size_type i = 0; i < configure_->pattern.size(); ++i)
			{
				if (configure_->pattern[i] != specif_t::prefix())
				{
					CharT ch = configure_->pattern[i];
					out << ch;
				}
				else
				{
					size_type next = i + 1;
					size_type n = next;
					for (; n < configure_->pattern.size(); ++n)
					{
						if (configure_->pattern[n] == specif_t::prefix())
							break;
					}

					string_t specify(&configure_->pattern[next], n - next);
					if (specify.compare(specif_t::date()) == 0)
					{
						out << std::move(datetime::to_string<CharT>(datetime::facet_fmt<CharT>::date_fmt()));
					}
					else if (specify.compare(specif_t::time()) == 0)
					{
						out << std::move(datetime::to_string<CharT>(datetime::facet_fmt<CharT>::time_fmt()));
					}
					else if (specify.compare(specif_t::func()) == 0)
					{
						out << std::move(func);
					}
					else if (specify.compare(specif_t::tid()) == 0)
					{
						out << std::move(tid_fmt<CharT>::to_string());
					}
					else if (specify.compare(specif_t::level()) == 0)
					{
						out << std::move(level_fmt<CharT>::to_string(level));
					}
					else if (specify.compare(specif_t::message()) == 0)
					{
						out << std::move(string_util<CharT, Argc...>::format(format, std::forward<Argc>(arg)...));
					}

					i = n;
				}
			}
		}
		catch (std::out_of_range& e) 
		{
			std::cout << e.what() << std::endl;
			return;
		}

		worker_->Put(std::move(out.to_string()));
	}
	
private:
	struct Configure;
	std::unique_ptr<Configure> configure_;

	class Worker;
	std::unique_ptr<Worker> worker_;

	std::vector<std::shared_ptr<ReporterImpl<CharT>>> reporters_;
};

/**
	@brief Configure의 속성명을 Character Type에 따라 처리할수 있도록 분류합니다.

	@since 0.2.0
*/
template <typename CharT>
struct ConfigureFiled {};

template <>
struct ConfigureFiled<char>
{
	const char* reporting_level() const { return "reporting_level"; }
	const char* pattern() const { return "pattern"; }
	const char* filename() const { return "filename"; }
};

template <>
struct ConfigureFiled<wchar_t>
{
	const wchar_t* reporting_level() const { return L"reporting_level"; }
	const wchar_t* pattern() const { return L"pattern"; }
	const wchar_t* filename() const { return L"filename"; }
};

template <typename CharT>
struct ConfigureFormats
{
	using char_type = CharT;

	static const char_type reporting_level[15];
	static const char_type pattern[8];
	static const char_type filename[9];
};

template <class CharT>
const typename ConfigureFormats<CharT>::char_type
ConfigureFormats<CharT>::reporting_level[15] = { 'r', 'e', 'p', 'o', 'r', 't', 'i', 'n', 'g', '_', 'l', 'e', 'v', 'e', 'l' };

template <typename CharT>
struct Logger<CharT>::Configure
{
	void Load(std::string&& path);	

	std::basic_string<CharT> str;

	Level reporting_level;
	string_t pattern;

	string_t filename;
};

template <typename CharT>
void Logger<CharT>::Configure::Load(std::string&& path)
{
	try 
	{
		using ptree = boost::property_tree::basic_ptree<string_t, string_t>;

		ptree props;
		boost::property_tree::read_json(path, props);
		
		ConfigureFiled<CharT> filed;

		auto level_opt = props.get_optional<string_t>(filed.reporting_level());
		if (!level_opt)
			reporting_level = Level::Trace;
		else
			reporting_level = level_fmt<CharT>::get(level_opt.get());

		auto pattern_opt = props.get_optional<string_t>(filed.pattern());
		if (!pattern_opt)
			pattern = pattern_form<CharT>::get();
		else
			pattern = pattern_opt.get();
		
		filename = props.get<string_t>(filed.filename());
	}
	catch (std::exception& e) 
	{
		throw parse_configure_error(e.what());
	}
}

template <typename CharT>
class Logger<CharT>::Worker
{
public:
	using string_t = std::basic_string<CharT>;
	using complete_queue = tbb::concurrent_bounded_queue<string_t>;
	using reporters = std::vector<std::shared_ptr<ReporterImpl<CharT>>>;
	
	Worker();

	void Run(reporters& repot);
	void Stop();

	void Put(string_t&& message);

private:
	volatile bool is_run_;

	complete_queue complete_queue_;
	std::thread thread_handle_;
};

template <typename CharT>
Logger<CharT>::Worker::Worker()
	: is_run_(false)
{
}

template <typename CharT>
void Logger<CharT>::Worker::Run(reporters& repot)
{
	is_run_ = true;

	thread_handle_ = std::thread(
	[this, &repot]() 
	{
		try
		{
			while (is_run_)
			{
				string_t out;
				complete_queue_.pop(out);

				if (is_run_ == false)
					break;

				for (auto& reporter : repot) {
					reporter->Report(std::forward<string_t>(out));
				}
			}
		}
		catch (std::exception& e)
		{
			std::cout << e.what() << std::endl;
		}
	});
}

template <typename CharT>
void Logger<CharT>::Worker::Stop()
{
	if (is_run_ == false)
		return;

	is_run_ = false;
	
	complete_queue_.abort();

	thread_handle_.join();
}

template <typename CharT>
void Logger<CharT>::Worker::Put(string_t&& message) 
{
	if (is_run_ == false || message.empty() == true)
		return;

	complete_queue_.push(std::move(message));
}
}
}