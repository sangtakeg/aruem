﻿/**
	@file reporter.h

	@brief 로그 메시지 출력 대상을 지정합니다.

	@author 권상택
	@date 2016-09-09
	@since 0.1.0
*/
#pragma once

#include <string>
#include <fstream>
#include "formatter.h"
#include "console.h"

namespace aruem
{
namespace log
{
/**
	@class ReporterImpl

	@brief  로그 메시지 출력 대상을 지정합니다.

	로그 출력의 경우 파일, 표준 출력, 원격지 전송 등 여러가지가 될수 있습니다.\n\n
	그렇기 때문에 유저는 ReporterImpl을 상속 받아 원하는 출력 대상을 지정 후 Logger::Attach 메쏘드를 이용해 등록 할수 있습니다.\n\n
	Logger::Initialize를 호출하게되면 기본적으로 파일과 표준 출력이 등록됩니다.

	@author 권상택
	@date 2016-09-09
	@since 0.1.0
*/
template <typename CharT>
class ReporterImpl
{
public:
	using string_t = std::basic_string<CharT>;

	ReporterImpl() {}
	virtual ~ReporterImpl() = default;

	virtual void Report(string_t&& message) = 0;
};

/**
	@class FileReporter

	@brief 로그 메시지를 받아 파일에 출력합니다.

	출력할 파일의 경로 및 파일명을 filename 메소드를 통해 전달받습니다.\n\n
	Report 메소드에서는 파일명을 생성해 현재 출력 대상 파일이 아닐 경우 새로운 파일을 생성하여 출력합니다.

	@author 권상택
	@date 2016-09-09
	@since 0.1.0
*/
template <typename CharT>
class FileReporter : public ReporterImpl<CharT>
{
public:
	using fstream = std::basic_fstream<CharT>;

	FileReporter() {}
	virtual ~FileReporter()
	{
		if (file_handle_.is_open())
		{
			file_handle_.close();
		}
	}

	/**
		@brief 파일 명을 전달 받습니다.

		@since 0.1.0
	*/
	void filename(string_t& filename) { filename_ = filename; }

	/**
		@brief 파일에 로그를 출력합니다.

		현재 오픈된 파일명이 없거나 변경됐을 경우 새로운 파일을 생성하여 출력합니다.

		@since 0.1.0
	*/
	virtual void Report(string_t&& message)
	{
		string_t new_filename = datetime::to_string<CharT>(std::forward<string_t>(filename_));

		if (open_filename_.empty() || open_filename_.compare(new_filename) != 0)
		{
			if (file_handle_.is_open())
				file_handle_.close();

			file_handle_.open(new_filename, fstream::out | fstream::app);
			if (file_handle_.is_open())
				open_filename_ = new_filename;
		}

		file_handle_.write(message.c_str(), message.length());
		file_handle_.flush();
	}

private:
	string_t filename_; ///< Configure filename 세팅값

	string_t open_filename_; ///< 현재 오픈된 파일명
	fstream file_handle_; ///< 현재 오픈된 파일 핸들
};

/**
	@class StandardOutReporter

	@brief 표준 출력으로 로그를 출력합니다.

	@author 권상택
	@date 2016-09-09
	@since 0.1.0
*/
template <typename CharT>
class StandardOutReporter : public ReporterImpl<CharT>
{
public:
	StandardOutReporter() {}
	virtual ~StandardOutReporter() = default;

	/**
		@brief 메시지를 표준 출력으로 출력합니다.

		@since 0.1.0
	*/
	virtual void Report(string_t&& message)
	{
		Console<CharT>::out(message);
	}
};
}
}