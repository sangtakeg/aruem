﻿/**
	@file level.h

	@brief 로그 레벨

	@author 권상택
	@date 2016-09-09
	@since 0.1.0
*/
#pragma once

namespace aruem
{
namespace log
{
/**
	@brief 로그 레벨 정의
*/
enum class Level {
	Unkown = 0, ///< 알수 없는 레벨
	Trace, ///< 레벨 1
	Debug, ///< 레벨 2
	Info, ///< 레벨 3
	Warn, ///< 레벨 4
	Error ///< 레벨 5
};
}
}

typedef aruem::log::Level Level;
