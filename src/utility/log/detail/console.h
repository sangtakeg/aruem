﻿/**
	@file console.h

	@brief character 타입에 따른 표준 출력

	@author 권상택
	@date 2016-09-09
	@since 0.1.0
*/
#pragma once

#include <iostream>

namespace aruem
{
namespace log
{
template <typename CharT>
struct Console {};

template <>
struct Console<char>
{
	static void out(std::string& message)
	{
		std::cout << message;
	}
};

template <>
struct Console<wchar_t>
{
	static void out(std::wstring& message)
	{
		std::wcout << message;
	}
};
}
}
