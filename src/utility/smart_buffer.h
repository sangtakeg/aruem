/**
	@file smart_buffer.h

	@brief 요구된 버퍼 크기에 따라 알맞는 버퍼를 할당합니다.

	@author 권상택
	@date 2017-09-29
	@since 0.2.0
*/
#pragma once

#include <memory>
#include <boost/aligned_storage.hpp>

/**
	@class SmartBuffer

	@brief 요구된 버퍼 크기에 따라 알맞는 버퍼를 할당합니다.

	지정된 Size 크기 이하의 버퍼를 요구했을 경우 미리 할당된 고정 버퍼를 반환하며 고정 버퍼보다 큰 사이즈의
	버퍼를 요구할 경우 메모리를 동적 할당하여 반환합니다.

	SmartBuffer는 Receive Buffer 목적으로 작성되었습니다.

	@author 권상택
	@date 2017-09-29
	@since 0.2.0
*/
template <std::size_t Size>
class SmartBuffer
{
public:
	BOOST_STATIC_CONSTANT(
		std::size_t, 
		size = Size	
	);

	using FixedBuffer = boost::aligned_storage<Size>;
	using DynamicBuffer = std::shared_ptr<uint8_t>;

	/**
		@brief 버퍼의 상태

		@author 권상택
		@since 0.2.0
	*/
	enum class Status : int32_t
	{
		Idle, ///< 버퍼가 사용되지 않고 있음
		Active ///< 버퍼가 할당되어 사용 중
	};

	SmartBuffer()
		: use_dynamic_buf_(false)
		, status_(Status::Idle)
		, length_(0)
	{
	}

	SmartBuffer(const SmartBuffer& right) 
	{
		Clone(right);
	}
	
	~SmartBuffer()
	{
		Deallocate();

		use_dynamic_buf_ = false;
		status_ = Status::Idle;
		length_ = 0;
	}

	void operator=(const SmartBuffer& right)
	{
		Clone(right);
	}

	const Status& status() const
	{
		return status_;
	}

	/**
		@brief 할당된 버퍼가 동적할당된 버퍼인지 검사합니다.

		@return 내부 버퍼가 동적할당됐을 경우 true를 그렇지 않을 경우 false를 반환합니다.
		@since 0.2.0
	*/
	bool IsUsedDynamicBuffer() const
	{
		return use_dynamic_buf_;
	}

	/**
		@brief IsUsedDynamicBuffer가 true일 경우 동적 할당된 버퍼 객체를 반환합니다.

		@return IsUsedDynamicBuffer가 true일 경우 동적 할당 버퍼를 그렇지 않을 경우 nullptr을 반환합니다.
		@since 0.2.0
	*/
	DynamicBuffer GetDynamicBuffer() const
	{
		if (IsUsedDynamicBuffer() == false) 
			return nullptr;

		return dynamic_buf_;
	}
	
	/**
		@brief 사용중인 버퍼를 반환합니다.

		@return Allocate또는 Reallocate되지 않았을 경우 nullptr을 그렇지 않을 경우 내부 할당된 버퍼를 반환합니다.
		@since 0.2.0
	*/
	const uint8_t* GetDataConst() const
	{
		if (status_ == Status::Idle)
			return nullptr;

		if (use_dynamic_buf_ == true)
		{
			return dynamic_buf_.get();
		}

		return static_cast<const uint8_t*>(fixed_buf_.address());
	}

	/**
		@brief 사용중인 버퍼를 반환합니다.

		@return Allocate또는 Reallocate되지 않았을 경우 nullptr을 그렇지 않을 경우 내부 할당된 버퍼를 반환합니다.
		@since 0.2.0
	*/
	uint8_t* GetData()
	{
		if (status_ == Status::Idle) 
			return nullptr;

		if (use_dynamic_buf_ == true) 
		{
			return dynamic_buf_.get();
		}

		return static_cast<uint8_t*>(fixed_buf_.address());
	}

	/**
		@brief 할당된 버퍼 크기 조회

		@return 할당된 버퍼 크기
		@since 0.2.0
	*/
	const std::size_t& GetDataLength() const
	{
		return length_;
	}

	/**
		@brief 버퍼를 할당합니다.

		@param size 할당 하려는 버퍼 크기

		@return 할당된 버퍼를 반환합니다.
		@since 0.2.0
	*/
	uint8_t* Allocate(std::size_t size)
	{
		if (status_ == Status::Active)
			return GetData();

		status_ = Status::Active;
		length_ = size;

		// 사이즈가 맞지 않을 경우 동적 할당합니다.
		if (use_dynamic_buf_ == false && (size > FixedBuffer::size))
		{
			use_dynamic_buf_ = true;
			dynamic_buf_.reset(new uint8_t[size]);

			return dynamic_buf_.get();
		}

		return static_cast<uint8_t*>(fixed_buf_.address());
	}

	/**
		@brief 버퍼 크기를 재 할당 합니다.

		@param size 할당 하려는 버퍼 크기

		@return 할당된 버퍼를 반환합니다.
		@since 0.2.0
	*/
	uint8_t* Reallocate(std::size_t size)
	{
		// 한번도 할당된 적이 없는 상태일 경우 재할당이 아닌 기본 할당으로 처리합니다.
		if (status_ == Status::Idle)
			return Allocate(size);
		
		if (FixedBuffer::size >= size)
		{
			// 현재 지정된 사이즈 변경
			length_ = size;
			return static_cast<uint8_t*>(fixed_buf_.address());
		}

		// 기존 fixed buffer를 사용중이였을 경우 동적 버퍼로 전환이 필요합니다.
		if (use_dynamic_buf_ == false)
		{
			use_dynamic_buf_ = true;
		}

		dynamic_buf_.reset(new uint8_t[size]);
		length_ = size;

		return dynamic_buf_.get();
	}

	/**
		@brief 버퍼를 반환합니다.

		@since 0.2.0
	*/
	void Deallocate()
	{
		if (use_dynamic_buf_ == true)
		{
			dynamic_buf_.reset();
			use_dynamic_buf_ = false;
		}

		status_ = Status::Idle;
	}

private:
	void Clone(const SmartBuffer& right)
	{
		if (right.status() == Status::Idle) 
		{
			Allocate(right.GetDataLength());
		}
		else if (right.status() == Status::Active && right.GetDataLength() > Size)
		{
			Reallocate(right.GetDataLength());
		}

		status_ = right.status();
		use_dynamic_buf_ = right.IsUsedDynamicBuffer();
		length_ = right.GetDataLength();

		if (use_dynamic_buf_ == true)
		{
			dynamic_buf_ = right.GetDynamicBuffer();
		}
		else
		{
			memcpy(fixed_buf_.address(), right.GetDataConst(), right.GetDataLength());
		}
	}

	Status status_;

	std::size_t length_;

	bool use_dynamic_buf_;
	DynamicBuffer dynamic_buf_;

	FixedBuffer fixed_buf_;
};