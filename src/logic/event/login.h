﻿/**
	@file login.h

	@brief 서버에 연결된 유저의 연결 정보

	@author 권상택
	@date 2016-09-09
	@since 0.1.0
*/
#pragma once

#include "network/event.h"

namespace aruem
{
namespace logic
{
/**
	@file login.h

	@brief 접속한 유저의 접속 채널

	@author 권상택
	@date 2016-09-09
	@since 0.1.0
*/
class Login : public network::Event
{
public:
	virtual void Process(uint32_t sid, const uint8_t* data, size_t bytes, void* tag)
	{
	}
};
} // namespace logic
} // namespace aruem