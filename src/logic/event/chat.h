﻿/**
	@file login.h

	@brief 채널에 입장한 유저들에게 메시지를 전송합니다.

	@author 권상택
	@date 2016-09-09
	@since 0.1.0
*/
#pragma once

#include "network/event.h"

namespace aruem
{
namespace logic
{
/**
	@file login.h

	@brief 채널에 입장한 유저들에게 메시지를 전송합니다.

	@author 권상택
	@date 2016-09-09
	@since 0.1.0
*/
	/*
#define DECLARE_PROCESS_CHAT virtual void Process(uint32_t sid, const uint8_t* data, size_t bytes, void* tag) { \
	ByteBuffer buffer(data, bytes); \
	std::wstring pid << buffer; \
	std::wstring version << buffer; \
	Process(sid, pid, version);
} \
*/
class Chat : public network::Event
{
public:
	//DECLARE_PROCESS_CHAT;

	virtual void Process(uint32_t sid, const uint8_t* data, size_t bytes, void* tag)
	{
	}
};
} // namespace logic
} // namespace aruem
/*
// 서버 입장에서는 Receive 지만 클라 입장에서는 Send다.
[Mode=sender,receiver]
namespace C2S {
	Chat(std::wstring& pid, std::wstring& version);
}

[mode=sender,receiver, language=cs,c++]
namespace S2C {
	ChatResult(int32_t error_code);
}

namespace C2S 
{
enum class PID
{
	Unknown,
	ChatResult,
};
}

class S2C : public TransferProxy {
public:
	void ChatResult(uint32_t sid, PHM phm, int32_t error_code) 
	{
		ByteBuffer buffer;
		buffer << error_code;
		
		Header header;
		header.id = PID::ChatResult;
		header.is_compress = 1;
		header.is_encrypt = 1;
		header.packet_handling_mode = phm;
		header.data_length = buffer.GetLength();

		Send(sid, header, buffer);
	}
}
*/