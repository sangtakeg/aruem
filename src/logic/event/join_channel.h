﻿/**
	@file login.h

	@brief 채널에 입장 합니다.

	@author 권상택
	@date 2016-09-09
	@since 0.1.0
*/
#pragma once

#include "network/event.h"

namespace aruem
{
namespace logic
{
/**
	@file login.h

	@brief 채널에 입장 합니다.

	@author 권상택
	@date 2016-09-09
	@since 0.1.0
*/
class JoinChannel : public network::Event
{
public:
	virtual void Process(uint32_t sid, const uint8_t* data, size_t bytes, void* tag)
	{
	}
};
} // namespace logic
} // namespace aruem