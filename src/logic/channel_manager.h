﻿/**
	@file channel_manager_impl.h

	@brief 채널 관리

	@author 권상택
	@date 2016-11-15
	@since 0.1.0
*/
#pragma once

#include <memory>
#include <vector>

namespace aruem
{
namespace logic
{
/**
	@file 

	@brief 

	@author 권상택
	@date 2016-09-09
	@since 0.1.0
*/
class Channel;
class ChannelManager
{
public:
	static const int kMaxChannelCount;

	ChannelManager();

	/**
	@brief 채널 입장

	@param channel_no 입장하고자하는 채널 번호
	@param session_id 유저의 세션 ID

	@return 정상적으로 입장됐을 경우 true, 그렇지 않을 경우 false 반환

	@since 0.1.0
	*/
	bool Join(int channel_no, long session_id);

	/**
	@brief 채널 퇴장

	@param channel_no 퇴장하고자 하는 채널 번호
	@param session_id 유저의 세션 ID

	@since 0.1.0
	*/
	void Leave(int channel_no, long session_id);

	/**
	@brief 채널에 접속 중인 유저의 세션ID 목록

	@param channel_no 조회 하고자 하는 채널 번호
	@param user_list 접속중인 유저의 세션ID 목록 전달

	@return 유저 목록 조회에 성공했을 경우 true, 그렇지 않을 경우 false를 반환

	@since 0.1.0
	*/
	bool GetUserList(int channel_no, std::vector<long>& user_list);

	/**
	@brief 채널에 접속 중인 유저수

	@param channel_no 조회 하고자 하는 채널 번호

	@return 현재 접속 중인 유저 수

	@since 0.1.0
	*/
	int GetUserCount(int channel_no);

private:
	class Impl;
	std::unique_ptr<Impl> pimpl_;
};
} // namespace logic
} // namespace aruem