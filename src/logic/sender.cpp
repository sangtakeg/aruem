#include "sender.h"

namespace aruem
{
namespace network
{
void Sender::ChatResult(int32_t sid, int32_t error_code)
{
	ChatResult(sid, PacketHandlingMode::Reliable, error_code);
}

void Sender::ChatResult(int32_t sid, PacketHandlingMode mode, int32_t error_code)
{
}

void Sender::ChatNotify(const int32_t* sid, std::size_t count, std::string&& msg)
{
	ChatNotify(sid, count, PacketHandlingMode::Reliable, std::forward<std::string>(msg));
}

void Sender::ChatNotify(const int32_t* sid, std::size_t count, PacketHandlingMode mode, std::string&& msg)
{
}
} // namespace network
} // namespace aruem