#pragma once

namespace aruem
{
namespace logic
{
enum PID
{
    UNKNOWN = 0,
	CHAT,
	JOIN_CHANNEL,
	LEAVE_CHANNEL,
	LOGIN,
};
} // namespace logic
} // namespace aruem
