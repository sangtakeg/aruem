﻿/**
	@file session.h

	@brief 서버에 연결된 유저의 연결 정보

	@author 권상택
	@date 2016-09-09
	@since 0.1.0
*/
#pragma once

#include <memory>
#include <vector>

namespace aruem
{
namespace logic
{
/**
	@file channel.h

	@brief 접속한 유저의 접속 채널

	@author 권상택
	@date 2016-09-09
	@since 0.1.0
*/
class Channel
{
public:
	/**
		@brief 생성자

		@param count 채널이 수용할 유저 수

		@since 0.1.0
	*/
	Channel(int capacity);
	~Channel();

	/**
	@brief 채널 입장

	@param session_id 유저의 세션 ID

	@return 정상적으로 입장됐을 경우 true, 그렇지 않을 경우 false 반환

	@since 0.1.0
	*/
	bool Join(long session_id);

	/**
	@brief 채널에서 퇴장

	@param session_id 유저의 세션 ID

	@since 0.1.0
	*/
	void Leave(long session_id);

	/**
	@brief 채널에 접속 중인 유저의 세션ID 목록 

	@param user_list 접속중인 유저의 세션ID 목록 전달

	@since 0.1.0
	*/
	void GetUserList(std::vector<long>& user_list);

	/**
	@brief 채널에 접속 중인 유저수

	@return 현재 접속 중인 유저 수

	@since 0.1.0
	*/
	int GetUserCount();

private:
	struct Impl;
	std::shared_ptr<Impl> pimpl_;
};
} // namespace network
} // namespace aruem