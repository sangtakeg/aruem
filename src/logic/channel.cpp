﻿#include "channel.h"
#include <set>
#include <tbb/spin_rw_mutex.h>

namespace aruem
{
namespace logic
{
struct Channel::Impl
{
	using UserList = std::set<long>;
	using RWMutext = tbb::spin_rw_mutex;
	
	int capacity;

	RWMutext rw_mutex;
	UserList user_list;
};

Channel::Channel(int capacity)
	: pimpl_(new Impl)
{
	pimpl_->capacity = capacity;
}

Channel::~Channel() 
{
}

bool Channel::Join(long session_id)
{
	Impl::RWMutext::scoped_lock lock(pimpl_->rw_mutex);

	if (pimpl_->user_list.size() >= pimpl_->capacity)
		return false;

	auto it = pimpl_->user_list.find(session_id);
	if (it != pimpl_->user_list.end())
		return true;

	pimpl_->user_list.insert(session_id);
	
	return true;
}

void Channel::Leave(long session_id)
{
	Impl::RWMutext::scoped_lock lock(pimpl_->rw_mutex);
	pimpl_->user_list.erase(session_id);
}

void Channel::GetUserList(std::vector<long>& user_list)
{
	Impl::RWMutext::scoped_lock lock(pimpl_->rw_mutex, false);

	for (long session_id : pimpl_->user_list)
	{
		user_list.push_back(session_id);
	}
}

int Channel::GetUserCount()
{
	Impl::RWMutext::scoped_lock lock(pimpl_->rw_mutex, false);
	return static_cast<int>(pimpl_->user_list.size());
}
} // namespace logic
} // namespace aruem