﻿#include "channel_manager.h"
#include "channel.h"
#include <vector>

namespace aruem
{
namespace logic
{
const int ChannelManager::kMaxChannelCount = 150;

class ChannelManager::Impl
{
public:
	Impl();

	Channel* GetChannel(int channel);

private:
	std::vector<std::unique_ptr<Channel>> channels_;
};

ChannelManager::Impl::Impl() 
{
	for (int i = 0; i < kMaxChannelCount; ++i)
	{
		channels_.push_back(std::make_unique<Channel>(100));
	}
}

Channel* ChannelManager::Impl::GetChannel(int channel_no)
{
	if (channel_no <= 0 || channel_no >= kMaxChannelCount)
		return nullptr;

	return channels_[channel_no].get();
}

ChannelManager::ChannelManager()
	:	pimpl_(new Impl)
{
}

bool ChannelManager::Join(int channel_no, long session_id)
{
	auto channel = pimpl_->GetChannel(channel_no);
	if (channel == nullptr)
		return false;

	return channel->Join(session_id);
}

void ChannelManager::Leave(int channel_no, long session_id)
{
	auto channel = pimpl_->GetChannel(channel_no);
	if (channel == nullptr)
		return;

	channel->Leave(session_id);
}

bool ChannelManager::GetUserList(int channel_no, std::vector<long>& user_list)
{
	auto channel = pimpl_->GetChannel(channel_no);
	if (channel == nullptr)
		return false;

	channel->GetUserList(user_list);
	return true;
}

int ChannelManager::GetUserCount(int channel_no)
{
	auto channel = pimpl_->GetChannel(channel_no);
	if (channel == nullptr)
		return 0;

	return channel->GetUserCount();
}
} // namespace logic
} // namespace aruem