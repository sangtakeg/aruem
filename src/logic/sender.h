#pragma once

#include <string>
#include "../network/transfer_proxy.h"
#include "../network/packet_handling_mode.h"

namespace aruem
{
namespace network
{
class Sender : public network::TransferProxy
{
public:
	static void ChatResult(int32_t sid, int32_t error_code);
	static void ChatResult(int32_t sid, PacketHandlingMode mode, int32_t error_code);

	static void ChatNotify(const int32_t* sid, std::size_t count, std::string&& msg);
	static void ChatNotify(const int32_t* sid, std::size_t count, PacketHandlingMode mode, std::string&& msg);
};
} // namespace network
} // namespace aruem