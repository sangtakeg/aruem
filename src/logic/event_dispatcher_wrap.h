#pragma once

#include <memory>
#include "network/event_dispatcher.h"
#include "pid.h"
#include "event/chat.h"
#include "event/join_channel.h"
#include "event/leave_channel.h"
#include "event/login.h"

namespace aruem
{
namespace logic
{
class EventDispatcherWrap
{
public:
    EventDispatcherWrap()
        : event_dispatcher_(new network::EventDispatcher)
    {
		event_dispatcher_->Register(PID::CHAT, std::make_shared<Chat>());
		event_dispatcher_->Register(PID::JOIN_CHANNEL, std::make_shared<JoinChannel>());
		event_dispatcher_->Register(PID::LEAVE_CHANNEL, std::make_shared<LeaveChannel>());
		event_dispatcher_->Register(PID::LOGIN, std::make_shared<Login>());
    }

    std::shared_ptr<network::EventDispatcher>& event_dispatcher()
    {
        return event_dispatcher_;
    }

private:
    std::shared_ptr<network::EventDispatcher> event_dispatcher_;
};
} // namespace logic
} // namespace aruem
